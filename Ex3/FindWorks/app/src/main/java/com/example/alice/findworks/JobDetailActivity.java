package com.example.alice.findworks;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.Text;

import java.util.ArrayList;

public class JobDetailActivity extends AppCompatActivity {

    DatabaseHandler database;
    String url ="";
    ArrayList<String> jobDetail = new ArrayList<>();
    Job job;
    TextView textViewDate,textViewLv,textViewWk, textViewSkill ,textViewDes, textViewRq;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_detail);

        database = new DatabaseHandler(this);
        textViewDate = (TextView) findViewById(R.id.tv_date);
        textViewLv = (TextView) findViewById(R.id.tv_lv);
        textViewSkill = (TextView) findViewById(R.id.tv_sk);
        textViewWk = (TextView) findViewById(R.id.tv_wk);
        textViewDes = (TextView) findViewById(R.id.tv_job_des);
        textViewRq = (TextView) findViewById(R.id.tv_job_rep) ;


        Intent intent = getIntent();
        jobDetail = intent.getStringArrayListExtra(MainActivity.JOB_DETAIL);
//        jobArrayList.add(new Job(nameJob, nameCpn, location, srcImage, urlDetail));

        job = new Job(jobDetail.get(0), jobDetail.get(1), jobDetail.get(2), jobDetail.get(3), jobDetail.get(4));
        Log.e("aa", job.getNameJob());
        Log.e("aa", job.getNameCompany() );

        url = jobDetail.get(4);

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Document document =(Document) Jsoup.parse(response);

                if (document != null){
                    //
                    Elements els_summary = document.select("div.summary-content");

                    String date = els_summary.get(0).getElementsByClass("content").text();
                    textViewDate.setText(date);

                    String lv = els_summary.get(1).getElementsByClass("content").text();
                    textViewLv.setText(lv);

                    String work = els_summary.get(2).getElementsByClass("content").text();
                    textViewWk.setText(work);

                    String skill = els_summary.get(3).getElementsByClass("content").text();
                    textViewSkill.setText(skill);


                    Elements els_des = document.select("div.description");

                    Log.i("DES",els_des.get(0).text());
                    String des = els_des.get(0).text();
                    textViewDes.setText(des.replace(".",".\n"));


                    Elements els_req = document.select("div.requirements");
                    String req = els_req.get(0).text();
                    textViewRq.setText(req.replace(".",".\n").replace(":",":\n\n"));

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.save_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.save:
                saveJob();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveJob() {
        boolean check = false;
        ArrayList<Job> jobArrayList = new ArrayList<Job>();
        jobArrayList = database.getAllJob();

        //Log.e("Cai moi them: ", job.getNameJob());
        for (int i = 0; i < jobArrayList.size(); i++) {
            if(jobArrayList.get(i).getNameJob().equals(job.getNameJob())) {
                check = true;
                //Toast.makeText(this, "Có cái bị trùng rồi nè", Toast.LENGTH_SHORT).show();
                break;
            }
            //Log.e("NAME JOB ", jobArrayList.get(i).getNameJob());
        }
        //Log.e("Check =  ", Boolean.toString(check));
        if (check) {
            Toast.makeText(this, "Job này đã tồn tại trong danh sách yêu thích", Toast.LENGTH_SHORT).show();
        } else {
            database.addJob(job);
            Toast.makeText(this, "Luu thanh cong", Toast.LENGTH_SHORT).show();
        }

    }
}

