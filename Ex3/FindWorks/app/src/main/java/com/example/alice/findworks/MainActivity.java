package com.example.alice.findworks;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private final String TAG = "Main";
    private CustomerAdapterListView customerAdapterListView;

    private ListView listView;
    private EditText edtSearch;
    private Button btnSearch;
    private ArrayList<Job> jobArrayList, searchResult;
    public  static final  String URL = "https://www.vietnamworks.com/";
    public  static final  String JOB_DETAIL = "job";
    private String keyword = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.lv_job);
        edtSearch = findViewById(R.id.edt_search);
        btnSearch = findViewById(R.id.btn_search);

        jobArrayList = new ArrayList<Job>();
        searchResult = new ArrayList<>();


        RequestQueue requestQueue = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {

                String nameCpn ="",nameJob = "",srcImage ="",location ="",urlDetail = "";
                Document document =(Document) Jsoup.parse(response);

                if (document != null) {
                    Elements elements = document.select("div.hot-job");
                    Log.i("TAG1", String.valueOf(elements.size()));
                    int i = 0;
                    for (Element element : elements) {
                        Element element_nameJob = element.getElementsByClass("job-title").first();
                        Element element_nameCpn = element.getElementsByClass("company").first();
                        Element element_location = element.getElementsByClass("location").first();

                        Element element_imgCpn = element.getElementsByClass("logo").first();
                        Element element_linkDetail = element.getElementsByTag("a").first();
                        i++;
                        if (element_nameJob != null) {
                            nameJob = element_nameJob.text();
                        }

                        if (element_nameCpn != null) {
                            nameCpn = element_nameCpn.text();
                        }

                        if (element_location != null) {
                            location = element_location.text();

                        }

                        if (element_imgCpn != null) {
                            srcImage = element_imgCpn.attr("src");

                        }

                        if (element_linkDetail != null) {
                            urlDetail = element_linkDetail.attr("href");
                        }

                        jobArrayList.add(new Job(nameJob, nameCpn, location, srcImage, urlDetail));
                        Log.i("TAG1", jobArrayList.toString());
                        if (i == 20) break;
                    }
                    customerAdapterListView = new CustomerAdapterListView(jobArrayList, MainActivity.this);
                    Log.i("TAG2", String.valueOf(customerAdapterListView.getCount()));
                    Log.i("TAG3", customerAdapterListView.getItem(0).toString());

                    listView.setAdapter(customerAdapterListView);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(stringRequest);



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Job job = (Job) parent.getAdapter().getItem(position);
                ArrayList<String> jobDetail = new ArrayList<String>();
                jobDetail.add(job.getNameJob());
                jobDetail.add(job.getNameCompany());
                jobDetail.add(job.getLocation());
                jobDetail.add(job.getImgCompany());
                jobDetail.add(job.getUrlDetail());

                Intent  detail_intent = new Intent(MainActivity.this, JobDetailActivity.class);
                detail_intent.putExtra(JOB_DETAIL,jobDetail);
                startActivity(detail_intent);
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchResult.clear();
                keyword = edtSearch.getText().toString().toUpperCase();
                Log.e(TAG, keyword + " " + keyword.isEmpty() + " " );
                if (keyword.isEmpty()) {
                    customerAdapterListView.changeDataset(jobArrayList);
                } else {
                    for (Job job : jobArrayList) {
                        if (job.getNameJob().toUpperCase().contains(keyword)
                                || job.getNameCompany().toUpperCase().contains(keyword)
                                || job.getLocation().toUpperCase().contains(keyword)) {
                            searchResult.add(job);
                        }
                    }
                    customerAdapterListView.changeDataset(searchResult);
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_job, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.showjob:
                Intent intent = new Intent(this, MyJobActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
