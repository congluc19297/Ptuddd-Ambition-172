package com.example.alice.findworks;

public class Job {
    private String id = "";
    private String nameJob;
    private String nameCompany;
    private String location;
    private String imgCompany;
    private String urlDetail;

    public Job(String nameJob, String nameCompany, String location, String imgCompany, String urlDetail) {
        this.nameJob = nameJob;
        this.nameCompany = nameCompany;
        this.location = location;
        this.imgCompany = imgCompany;
        this.urlDetail = urlDetail;
    }

    public Job(String id, String nameJob, String nameCompany, String location, String imgCompany, String urlDetail) {
        this.id = id;
        this.nameJob = nameJob;
        this.nameCompany = nameCompany;
        this.location = location;
        this.imgCompany = imgCompany;
        this.urlDetail = urlDetail;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNameJob() {
        return nameJob;
    }

    public void setNameJob(String nameJob) {
        this.nameJob = nameJob;
    }

    public String getNameCompany() {
        return nameCompany;
    }

    public void setNameCompany(String nameCompany) {
        this.nameCompany = nameCompany;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getImgCompany() {
        return imgCompany;
    }

    public void setImgCompany(String imgCompany) {
        this.imgCompany = imgCompany;
    }

    public String getUrlDetail() {
        return urlDetail;
    }

    public void setUrlDetail(String urlDetail) {
        this.urlDetail = urlDetail;
    }

}
