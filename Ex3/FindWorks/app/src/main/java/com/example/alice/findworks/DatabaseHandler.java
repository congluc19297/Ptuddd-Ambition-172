package com.example.alice.findworks;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "jobManager";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "jobs";

    //    private String nameJob;
    //    private String nameCompany;
    //    private String location;
    //    private String imgCompany;
    //    private String urlDetail;
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "nameJob";
    private static final String KEY_LOCATION = "location";
    private static final String KEY_COMPANY = "nameCompany";
    private static final String KEY_IMG_COMPANY = "imgCompany";
    private static final String KEY_URL = "urlDetail";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String create_jobs_table = String.format("CREATE TABLE %s(%s INTEGER PRIMARY KEY, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT)",
                TABLE_NAME, KEY_ID, KEY_NAME, KEY_LOCATION, KEY_COMPANY, KEY_IMG_COMPANY, KEY_URL);
        Log.e("database: ", create_jobs_table);
        db.execSQL(create_jobs_table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String drop_jobs_table = String.format("DROP TABLE IF EXISTS %s", TABLE_NAME);
        db.execSQL(drop_jobs_table);

        onCreate(db);
    }

    public void addJob(Job job) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, job.getNameJob());
        values.put(KEY_COMPANY, job.getNameCompany());
        values.put(KEY_LOCATION, job.getLocation());
        values.put(KEY_IMG_COMPANY, job.getImgCompany());
        values.put(KEY_URL, job.getUrlDetail());

        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public Job getJob(int jobId) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME, null, KEY_ID + " = ?", new String[] { String.valueOf(jobId) },null, null, null);
        if(cursor != null)
            cursor.moveToFirst();
        Job job = new Job(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5));
        return job;
    }

    public ArrayList<Job> getAllJob() {
        ArrayList<Job>  jobList = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while(cursor.isAfterLast() == false) {
            Job job = new Job(cursor.getString(0), cursor.getString(1), cursor.getString(3), cursor.getString(2), cursor.getString(4), cursor.getString(5));
            jobList.add(job);
            cursor.moveToNext();
        }
        return jobList;
    }

    public void deleteJob(int jobId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, KEY_ID + " = ?", new String[] { String.valueOf(jobId) });
        db.close();
    }
}
