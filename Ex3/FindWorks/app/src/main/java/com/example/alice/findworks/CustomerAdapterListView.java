package com.example.alice.findworks;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CustomerAdapterListView extends BaseAdapter {

    private ArrayList<Job> jobArrayList = new ArrayList<>();
    private Context context;

    public CustomerAdapterListView(ArrayList<Job> jobArrayList, Context context) {
        this.jobArrayList.addAll(jobArrayList);
        this.context = context;
    }
    @Override
    public int getCount() {
        return jobArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return jobArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater  = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      convertView  = layoutInflater.inflate(R.layout.activity_job_item,null);
        TextView textViewNameJob = (TextView) convertView.findViewById(R.id.tv_name_job);
        TextView textViewCpn = (TextView) convertView.findViewById(R.id.tv_name_cpn);
        TextView textViewLocation = (TextView) convertView.findViewById(R.id.tv_location);

        ImageView imageViewJob = (ImageView) convertView.findViewById(R.id.img_job);

        Job job =(Job)getItem(position);

        textViewNameJob.setText(job.getNameJob());
        textViewCpn.setText(job.getNameCompany());
        textViewLocation.setText(job.getLocation());

        Glide.with(context)
                .load(job.getImgCompany())
                .into(imageViewJob);

        return convertView;
    }

    public void changeDataset(ArrayList<Job> new_data) {
        this.jobArrayList.clear();
        this.jobArrayList.addAll(new_data);
        notifyDataSetChanged();
    }
}
