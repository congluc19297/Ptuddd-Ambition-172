package com.ambition.moviereview.view;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

import com.ambition.moviereview.R;
import com.ambition.moviereview.controller.FireBaseHelper;
import com.ambition.moviereview.model.Movie;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.ambition.moviereview.utilities.Constants.MOVIE;

public class HomeActivity extends BaseActivity {

    private final String TAG = HomeActivity.class.getSimpleName();

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private EditText edtSearch;

    private ArrayList<Movie> movies = new ArrayList<>();
    private ArrayList<Movie> newest = new ArrayList<>();
    private ArrayList<Movie> trending = new ArrayList<>();
    private ArrayList<Movie> rating_highest = new ArrayList<>();


    private FireBaseHelper db_helper;
    // Write a message to the database
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference movie_ref = database.getReference(MOVIE);
    private String keyword = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        autoHideKeyboard(findViewById(R.id.root),this);
        matchHeaderId();
        db_helper = new FireBaseHelper(this);
        setupHeader(isLogged());
        matchViewId();
        loadData();
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        searching();
    }

    private void searching() {
        edtSearch = (EditText) findViewById(R.id.edt_search);
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filterMovie();
            }
        });
        viewPager.requestFocus();
    }

    private void filterMovie() {
        keyword = edtSearch.getText().toString().toUpperCase();
        ArrayList<Movie> temp = new ArrayList<>();
        for (Movie movie: movies) {
            if (movie.getName().toUpperCase().contains(keyword) || movie.getProduction_company().toUpperCase().contains(keyword)) {
                temp.add(movie);
            }
        }
        processData(temp);
    }

    private void matchViewId() {
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.container);
        viewPager.requestFocus();
    }

    public void setupViewPager(ViewPager view_pager) {
        Log.e(TAG, newest.size() + "");
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new MainFragment(newest), "Mới nhất");
        adapter.addFragment(new MainFragment(trending), "Phổ biến");
        adapter.addFragment(new MainFragment(rating_highest), "Đánh giá cao");
        view_pager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void loadData() {
        showProgressDialog();
        ValueEventListener movieListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                movies.clear();
                Iterable<DataSnapshot> movieSnapshot = dataSnapshot.getChildren();
                for (DataSnapshot movie : movieSnapshot) {
                    try {
                        Movie m = movie.getValue(Movie.class);
                        movies.add(m);
                        Log.e("movie_name: ", m.getName());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                Log.e(TAG, "movies: " + movies.size());
                processData(movies);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "loadMovie: failed - ", databaseError.toException());
            }
        };
        movie_ref.addValueEventListener(movieListener);
    }

    class StringDateComparator implements Comparator<String>
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        public int compare(String lhs, String rhs)
        {
            try {
                return dateFormat.parse(lhs).compareTo(dateFormat.parse(rhs));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return 0;
        }

    }

    private void changeArray(ArrayList<Movie> source, ArrayList<Movie> destination) {
        for (int i=0; i<source.size(); i++) {
            destination.add(source.get(i));
        }
    }

    private void processData(ArrayList<Movie> movies) {
        newest.clear();
        trending.clear();
        rating_highest.clear();
        ArrayList<Movie> temp = new ArrayList<>();
        changeArray(movies, temp);
        // Sort movies by time

        for (int i = temp.size() - 1; i >=0; i--) {
            newest.add(temp.get(i));
        }

        temp.clear();
        changeArray(movies, temp);

        // Sort by number of comments
        ArrayList<Integer> tempComment = new ArrayList<>();
        for (Movie movie : movies) {
            tempComment.add(movie.getComments().size());
        }
        Collections.sort(tempComment, Collections.reverseOrder());
        for (int comments : tempComment) {
            for (int i = 0; i < temp.size(); i++) {
                if (comments == temp.get(i).getComments().size()) {
                    // Remove to sure data not duplicated
                    trending.add(temp.get(i));
                    temp.remove(i);
                    break;
                }
            }
        }
        tempComment.clear();
        temp.clear();
        changeArray(movies, temp);

        // Sort by rating
        ArrayList<Float> tempRating = new ArrayList<>();
        for (Movie movie : movies) {
            tempRating.add(movie.getAvg_rating());
        }
        Collections.sort(tempRating, Collections.reverseOrder());
        for (float rating : tempRating) {
            for (int i = 0; i < temp.size(); i++) {
                if (rating == temp.get(i).getAvg_rating()) {
                    // Remove to sure data not duplicated
                    rating_highest.add(temp.get(i));
                    temp.remove(i);
                    break;
                }
            }
        }
        tempComment.clear();
        temp.clear();
        hideProgressDialog();
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

}
