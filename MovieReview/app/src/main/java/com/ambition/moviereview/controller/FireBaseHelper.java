package com.ambition.moviereview.controller;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.ambition.moviereview.R;
import com.ambition.moviereview.model.Account;
import com.ambition.moviereview.model.Comment;
import com.ambition.moviereview.model.Movie;
import com.ambition.moviereview.model.Person;
import com.ambition.moviereview.view.BaseActivity;
import com.ambition.moviereview.view.HomeActivity;
import com.ambition.moviereview.view.UserInfoActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import static com.ambition.moviereview.utilities.Constants.ACCOUNT;
import static com.ambition.moviereview.utilities.Constants.ACTOR;
import static com.ambition.moviereview.utilities.Constants.DIRECTOR;
import static com.ambition.moviereview.utilities.Constants.MOVIE;

/**
 * Created by nhan on 06/05/2018.
 */

public class FireBaseHelper extends BaseActivity {
    private final String TAG = FireBaseHelper.class.getSimpleName();
    // Realtime database
    private final String TAG_LOG = FireBaseHelper.class.getSimpleName();
    private final FirebaseDatabase database = FirebaseDatabase.getInstance();
    private final DatabaseReference account_ref = database.getReference(ACCOUNT);
    private final DatabaseReference movie_ref = database.getReference(MOVIE);
    private final DatabaseReference actor_ref = database.getReference(ACTOR);
    private final DatabaseReference director_ref = database.getReference(DIRECTOR);
    public boolean is_success = false;


    // Authentication
    private final FirebaseAuth mAuth = FirebaseAuth.getInstance();

    // Context
    public Context context;
    public Activity activity;
    public ProgressDialog mProgressDialog;


    public FireBaseHelper(Context context) {
        this.context = context;
        this.activity = (Activity) context;
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Đang xử lí");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public boolean addAccount(Account new_account, String uid) {
        try {
            Log.e(TAG_LOG, "Them moi account len Firebase");
            account_ref.child(uid).setValue(new_account);
            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String addMovie(Movie new_movie) {
        try {
            Log.e(TAG_LOG, "Them moi movie len Firebase / ten phim: "+new_movie.getName());
            String movie_id = movie_ref.push().getKey();
            movie_ref.child(movie_id).setValue(new_movie);
            movie_ref.child(movie_id).child("id").setValue(movie_id);
            return movie_id;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean addComment(Comment new_comment, Movie movie) {
        ArrayList<Comment> comments = movie.getComments();
        comments.add(new_comment);
        movie.setComments(comments);
        int sum = 0;
        int count = 0;
        for(Comment item: comments) {
            sum += item.getRating_level();
            count++;
        }
        float rating = sum / count;
        movie.setAvg_rating(rating);
        movie_ref.child(movie.getId()).setValue(movie);

        return true;
    }

    public boolean addActor(Person new_person) {
        try {
            String person_id = actor_ref.push().getKey();
            new_person.setId(person_id);
            actor_ref.child(person_id).setValue(new_person);
            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean updateActor(String id, Person new_person) {
        try {
            new_person.setId(id);
            actor_ref.child(id).setValue(new_person);
            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    public boolean addDirector(Person new_person) {
        try {
            String person_id = actor_ref.push().getKey();
            new_person.setId(person_id);
            director_ref.child(person_id).setValue(new_person);
            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean updateDirector(String id, Person new_person) {
        try {
            new_person.setId(id);
            director_ref.child(id).setValue(new_person);
            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void addNewAccount(String email, String password, final Account new_account) {
        Log.e(TAG, "addNewAccount");
        showProgressDialog();
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.e(TAG_LOG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            addAccount(new_account, user.getUid());
                            Toast.makeText(context, "Đăng ký thành công", Toast.LENGTH_SHORT).show();
                            // Login và trở về màn hình chính
                            Intent intent = new Intent(context, HomeActivity.class);
                            context.startActivity(intent);
                            activity.finish();
                            // Update UI

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.e(TAG_LOG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(context, context.getResources().getString(R.string.signin_failed),
                                    Toast.LENGTH_SHORT).show();
                        }
                        hideProgressDialog();
                    }
                });
    }

    public boolean resetPassword(String email) {
        showProgressDialog();
        mAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.e(TAG_LOG, "Email sent.");
                            Toast.makeText(context, "Vui lòng kiểm tra email để đặt lại mật khẩu", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Log.e(TAG_LOG, "Failed to send email");
                            Toast.makeText(context, "Đã có lỗi xảy ra. Vui lòng thử lại", Toast.LENGTH_SHORT).show();
                        }
                        hideProgressDialog();
                    }
                });
        return true;
    }

    public void signIn(String email, String password) {
        showProgressDialog();
        // [START sign_in_with_email]
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.e(TAG, "signInWithEmail:success");
                            globalUser = mAuth.getCurrentUser();
                            Log.e(TAG, globalUser.getUid());
                            Intent home_intent = new Intent(context, HomeActivity.class);
                            context.startActivity(home_intent);
                            activity.finish();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.e(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(context, "Đăng nhập thất bại. Vui lòng thử lại",
                                    Toast.LENGTH_SHORT).show();
                        }
                        hideProgressDialog();
                    }
                });
        // [END sign_in_with_email]
    }

    public void changePassword(String email, String oldPass, final String newPass) {
        AuthCredential credential = EmailAuthProvider
                .getCredential(email, oldPass);
        showProgressDialog();

        // Prompt the user to re-provide their sign-in credentials
        globalUser.reauthenticate(credential)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            globalUser.updatePassword(newPass).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Log.e(TAG, "Password updated");
                                        Toast.makeText(context, "Cập nhật mật khẩu thành công", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(context, UserInfoActivity.class);
                                        context.startActivity(intent);
                                        activity.finish();
                                    } else {
                                        Log.e(TAG, "Error password not updated");
                                        Toast.makeText(context, "Đã có lỗi xảy ra. Thử lại sau.", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        } else {
                            Log.e(TAG, "Error auth failed");
                            Toast.makeText(context, "Mật khẩu cũ không chính xác", Toast.LENGTH_SHORT).show();
                        }
                        hideProgressDialog();
                    }
                });
    }

    public void signOut() {
        mAuth.signOut();
        globalUser = null;
        Intent home_intent = new Intent(context, HomeActivity.class);
        context.startActivity(home_intent);
        activity.finish();
    }

    public void updateMovie(String movieId, Movie new_movie) {
        try {
            new_movie.setId(movieId);
            Log.e(TAG_LOG, "Them moi movie len Firebase / ten phim: "+new_movie.getName());
            movie_ref.child(movieId).setValue(new_movie);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
