package com.ambition.moviereview.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ambition.moviereview.R;
import com.ambition.moviereview.controller.FireBaseHelper;
import com.ambition.moviereview.utilities.Utilities;
import com.balysv.materialripple.MaterialRippleLayout;

public class SignInActivity extends BaseActivity {

    private LinearLayout lnRoot;
    private MaterialRippleLayout btnBack;
    private MaterialRippleLayout btnHome;
    private TextView tvTitle;
    private EditText edtEmail;
    private EditText edtPassword;
    private Button btnSignIn;
    private Button btnSignUp;
    private Button btnForgotPassword;
    private FireBaseHelper db_helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        // Init dbhelper context and activity
        db_helper = new FireBaseHelper(this);
        // My function
        matchViewId();
        autoHideKeyboard(lnRoot, this);
        customFont();
        setEvent();
    }

    private void matchViewId() {
        lnRoot = (LinearLayout) findViewById(R.id.root);
        btnBack = (MaterialRippleLayout) findViewById(R.id.btn_back);
        btnHome = (MaterialRippleLayout) findViewById(R.id.btn_home);
        tvTitle = (TextView) findViewById(R.id.tv_appname);
        edtEmail = (EditText) findViewById(R.id.edt_username);
        edtEmail.getBackground().clearColorFilter();
        edtPassword = (EditText) findViewById(R.id.edt_password);
        edtPassword.getBackground().clearColorFilter();
        btnSignIn = (Button) findViewById(R.id.btn_login);
        btnSignUp = (Button) findViewById(R.id.btn_sign_up);
        btnForgotPassword = (Button) findViewById(R.id.btn_resetpassword);
    }

    private void setEvent() {
        // Get back previous activity
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // Return home activity
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home_intent = new Intent(SignInActivity.this, HomeActivity.class);
                startActivity(home_intent);
                finish();
            }
        });

        // SignIn
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO SignIn function
                String email = edtEmail.getText().toString();
                String password = edtPassword.getText().toString();
                if (email.isEmpty()) {
                    Toast.makeText(SignInActivity.this, "Vui lòng điền email", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (password.isEmpty()) {
                    Toast.makeText(SignInActivity.this, "Vui lòng điền mật khẩu", Toast.LENGTH_SHORT).show();
                    return;
                }
                db_helper.signIn(email, password);
            }
        });

        // If not have account yet, create one
        // Go to sign up activity
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signup_intent = new Intent(SignInActivity.this, SignUpActivity.class);
                startActivity(signup_intent);
            }
        });

        btnForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = edtEmail.getText().toString();
                if (email.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Vui lòng điền vào email",
                            Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    db_helper.resetPassword(email);
                }
            }
        });
    }

    /** Change font */
    private void customFont() {
        Context context = this.getApplicationContext();
        Utilities.setFontFamily("Lobster-Regular.ttf", tvTitle, context);
        Utilities.setFontFamily("RobotoSlab-Regular.ttf", edtEmail, context);
        Utilities.setFontFamily("RobotoSlab-Regular.ttf", edtPassword, context);
        Utilities.setFontFamily("RobotoSlab-Bold.ttf", btnSignIn, context);
        Utilities.setFontFamily("RobotoSlab-Bold.ttf", btnSignUp, context);
        Utilities.setFontFamily("RobotoSlab-Bold.ttf", btnForgotPassword, context);
    }


}
