package com.ambition.moviereview.view;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.widget.TextView;

import com.ambition.moviereview.R;
import com.ambition.moviereview.utilities.Utilities;
import com.google.firebase.auth.FirebaseAuth;

public class SplashScreenActivity extends BaseActivity {

    private final int SPLASH_DISPLAY_LENGTH = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        /** Link view by ID */
        TextView mTextViewAppName = (TextView) findViewById(R.id.tv_appname);
        TextView mTextViewSubTitle = (TextView) findViewById(R.id.tv_subtitle);

        /** Change font */
        Context context = this.getApplicationContext();
        Utilities.setFontFamily("Lobster-Regular.ttf", mTextViewAppName, context);
        Utilities.setFontFamily("Lobster-Regular.ttf", mTextViewSubTitle, context);

        // Check is logged
        globalUser = FirebaseAuth.getInstance().getCurrentUser();

        /** Handler to delay time to display Splash screen **/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /** Create an Intent that will start the Menu-Activity. **/
                Intent mainIntent = new Intent(SplashScreenActivity.this, HomeActivity.class);
                startActivity(mainIntent);
                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
