package com.ambition.moviereview.controller;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ambition.moviereview.R;
import com.ambition.moviereview.model.Movie;
import com.ambition.moviereview.utilities.ItemClickListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

/**
 * Created by nhan on 18/05/2018.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {

    private ArrayList<Movie> mMovies = new ArrayList<>();
    private Context context;
    private ItemClickListener listener;

    public MovieAdapter(ArrayList<Movie> movies, Context ctx, ItemClickListener listener) {
        this.mMovies = movies;
        Log.e("Adapter:", mMovies.size() + "");
        this.context = ctx;
        this.listener = listener;
    }

    // Usually involves inflating a layout from XML and returning the holder
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        // Inflate the custom layout
        View movieView = inflater.inflate(R.layout.movie_item, parent, false);

        // Return a new holder instance
        final ViewHolder viewHolder = new ViewHolder(movieView);

        // Item click
        movieView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, viewHolder.getAdapterPosition());
            }
        });

        return viewHolder;
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // Get the data model based on position
        Movie movie = mMovies.get(position);

        // Set item views based on your views and data model

        // Movie name
        TextView mMovieName = holder.tvMovieName;
        mMovieName.setText(movie.getName());

        // List of director
        TextView mMovieDirector = holder.tvDirector;
        String directors = "Đạo diễn: ";
        for (String director: movie.getDirector()) {
            directors += director + " | ";
        }
        try {
            mMovieDirector.setText(directors);
        }
        catch (Exception e) {
            mMovieDirector.setText("Đạo diễn: Chưa có");
        }

        TextView mMovieGenre = holder.tvGenere;
        String genres = "Thể loại: ";
        if (movie.getGenres().size() != 0) {
            for (String genre : movie.getGenres()) {
                genres += genre + " | ";
            }
        }
        try {
            mMovieGenre.setText(genres);
        }
        catch (Exception e) {
            mMovieGenre.setText("Thể loại: Chưa có");
            Log.e("Adapter:", e.toString());
        }

        TextView mProduction = holder.tvProduction;
        mProduction.setText("Hãng sản xuất: " + movie.getProduction_company());

        TextView mRating = holder.tvRating;
        mRating.setText("Đánh giá: " + String.valueOf(movie.getAvg_rating()));

        final ImageView mThumbnail = holder.imgThumbnail;
        final ImageView mHolder = holder.imgHolder;
        Glide.with(context)
                .load(movie.getImageURL())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        mThumbnail.setBackgroundColor(context.getResources().getColor(R.color.white));
                        mHolder.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(mThumbnail);

        float rating = movie.getAvg_rating();
        Log.e("Rating:", rating + "");
        ImageView mStar1 = holder.imgStar1;
        ImageView mStar2 = holder.imgStar2;
        ImageView mStar3 = holder.imgStar3;
        ImageView mStar4 = holder.imgStar4;
        ImageView mStar5 = holder.imgStar5;

        if (rating < 0.5) {
            mStar1.setImageResource(R.drawable.ic_empty);
            mStar2.setImageResource(R.drawable.ic_empty);
            mStar3.setImageResource(R.drawable.ic_empty);
            mStar4.setImageResource(R.drawable.ic_empty);
            mStar5.setImageResource(R.drawable.ic_empty);
        } else if (rating >= 0.5f && rating < 0.75f) {
            mStar1.setImageResource(R.drawable.ic_star_half);
            mStar2.setImageResource(R.drawable.ic_empty);
            mStar3.setImageResource(R.drawable.ic_empty);
            mStar4.setImageResource(R.drawable.ic_empty);
            mStar5.setImageResource(R.drawable.ic_empty);
        } else if (rating >= 0.75 && rating < 1.25) {
            mStar1.setImageResource(R.drawable.ic_star_full);
            mStar2.setImageResource(R.drawable.ic_empty);
            mStar3.setImageResource(R.drawable.ic_empty);
            mStar4.setImageResource(R.drawable.ic_empty);
            mStar5.setImageResource(R.drawable.ic_empty);
        }
        else if (rating >= 1.25f && rating <1.75f) {
            mStar1.setImageResource(R.drawable.ic_star_full);
            mStar2.setImageResource(R.drawable.ic_star_half);
            mStar3.setImageResource(R.drawable.ic_empty);
            mStar4.setImageResource(R.drawable.ic_empty);
            mStar5.setImageResource(R.drawable.ic_empty);
        }
        else if (rating >= 1.75 && rating < 2.25) {
            mStar1.setImageResource(R.drawable.ic_star_full);
            mStar2.setImageResource(R.drawable.ic_star_full);
            mStar3.setImageResource(R.drawable.ic_empty);
            mStar4.setImageResource(R.drawable.ic_empty);
            mStar5.setImageResource(R.drawable.ic_empty);
        }
        else if (rating >= 2.25f && rating <2.75f) {
            mStar1.setImageResource(R.drawable.ic_star_full);
            mStar2.setImageResource(R.drawable.ic_star_full);
            mStar3.setImageResource(R.drawable.ic_star_half);
            mStar4.setImageResource(R.drawable.ic_empty);
            mStar5.setImageResource(R.drawable.ic_empty);
        }
        else if (rating >= 2.75 && rating < 3.25) {
            mStar1.setImageResource(R.drawable.ic_star_full);
            mStar2.setImageResource(R.drawable.ic_star_full);
            mStar3.setImageResource(R.drawable.ic_star_full);
            mStar4.setImageResource(R.drawable.ic_empty);
            mStar5.setImageResource(R.drawable.ic_empty);
        }
        else if (rating >= 3.25f && rating <3.75f) {
            mStar1.setImageResource(R.drawable.ic_star_full);
            mStar2.setImageResource(R.drawable.ic_star_full);
            mStar3.setImageResource(R.drawable.ic_star_full);
            mStar4.setImageResource(R.drawable.ic_star_half);
            mStar5.setImageResource(R.drawable.ic_empty);
        }
        else if (rating >= 3.75 && rating < 4.25) {
            mStar1.setImageResource(R.drawable.ic_star_full);
            mStar2.setImageResource(R.drawable.ic_star_full);
            mStar3.setImageResource(R.drawable.ic_star_full);
            mStar4.setImageResource(R.drawable.ic_star_full);
            mStar5.setImageResource(R.drawable.ic_empty);
        }
        else if (rating >= 4.25f && rating <4.75f) {
            mStar1.setImageResource(R.drawable.ic_star_full);
            mStar2.setImageResource(R.drawable.ic_star_full);
            mStar3.setImageResource(R.drawable.ic_star_full);
            mStar4.setImageResource(R.drawable.ic_star_full);
            mStar5.setImageResource(R.drawable.ic_star_half);
        }
        else {
            mStar1.setImageResource(R.drawable.ic_star_full);
            mStar2.setImageResource(R.drawable.ic_star_full);
            mStar3.setImageResource(R.drawable.ic_star_full);
            mStar4.setImageResource(R.drawable.ic_star_full);
            mStar5.setImageResource(R.drawable.ic_star_full);
        }
    }

    @Override
    public int getItemCount() {
        return mMovies.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imgThumbnail, imgHolder, imgStar1, imgStar2, imgStar3, imgStar4, imgStar5;
        public TextView tvMovieName, tvGenere, tvDirector, tvProduction, tvRating;

        public ViewHolder(View itemView) {
            super(itemView);
            imgThumbnail = (ImageView) itemView.findViewById(R.id.img_thumbnail);
            imgHolder = (ImageView) itemView.findViewById(R.id.img_holder);
            imgStar1 = (ImageView) itemView.findViewById(R.id.img_s1);
            imgStar2 = (ImageView) itemView.findViewById(R.id.img_s2);
            imgStar3 = (ImageView) itemView.findViewById(R.id.img_s3);
            imgStar4 = (ImageView) itemView.findViewById(R.id.img_s4);
            imgStar5 = (ImageView) itemView.findViewById(R.id.img_s5);
            tvMovieName = (TextView) itemView.findViewById(R.id.tv_movie_name);
            tvDirector = (TextView) itemView.findViewById(R.id.tv_director);
            tvGenere = (TextView) itemView.findViewById(R.id.tv_generes);
            tvProduction = (TextView) itemView.findViewById(R.id.tv_productor);
            tvRating = (TextView) itemView.findViewById(R.id.tv_rating);
        }
    }



}
