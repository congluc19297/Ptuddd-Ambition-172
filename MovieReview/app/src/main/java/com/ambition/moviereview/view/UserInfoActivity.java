package com.ambition.moviereview.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ambition.moviereview.R;
import com.ambition.moviereview.controller.FireBaseHelper;
import com.ambition.moviereview.model.Account;
import com.balysv.materialripple.MaterialRippleLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static com.ambition.moviereview.utilities.Constants.ACCOUNT;
import static com.ambition.moviereview.utilities.Constants.ADDRESS;
import static com.ambition.moviereview.utilities.Constants.BIRTHDAY;
import static com.ambition.moviereview.utilities.Constants.GENDER;
import static com.ambition.moviereview.utilities.Constants.NAME;
import static com.ambition.moviereview.utilities.Constants.PHONE;
import static com.ambition.moviereview.utilities.Constants.USERNAME;

public class UserInfoActivity extends BaseActivity {
    private final String TAG = UserInfoActivity.class.getSimpleName();
    private TextView tvName, tvUsername, tvEmail, tvPhone, tvAddress, tvGender, tvBirthDay;
    private LinearLayout lnEditInfo, lnChangePassword, lnSignout, lnContenManagement;
    private MaterialRippleLayout btnBack, btnHome;


    private FireBaseHelper db_helper;
    private final FirebaseDatabase database = FirebaseDatabase.getInstance();
    private final DatabaseReference account_ref = database.getReference(ACCOUNT);
    private Account account = new Account();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);

        // My function
        db_helper = new FireBaseHelper(this);
        matchViewsId();
        getUserInfo(globalUser.getUid());
        addEvent();
    }

    private void matchViewsId() {
        tvName = (TextView) findViewById(R.id.tv_name);
        tvUsername = (TextView) findViewById(R.id.tv_username);
        tvEmail = (TextView) findViewById(R.id.tv_email);
        tvPhone = (TextView) findViewById(R.id.tv_phone);
        tvAddress = (TextView) findViewById(R.id.tv_address);
        tvGender = (TextView) findViewById(R.id.tv_gender);
        tvBirthDay = (TextView) findViewById(R.id.tv_birthday);
        lnEditInfo = (LinearLayout) findViewById(R.id.ln_edit_info);
        lnChangePassword = (LinearLayout) findViewById(R.id.ln_change_password);
        lnContenManagement = (LinearLayout) findViewById(R.id.ln_my_content);
        lnSignout = (LinearLayout) findViewById(R.id.ln_sign_out);
        btnBack = (MaterialRippleLayout) findViewById(R.id.btn_back);
        btnHome = (MaterialRippleLayout) findViewById(R.id.btn_home);
    }

    private void getUserInfo(String uid) {
        ValueEventListener accountListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                account = dataSnapshot.getValue(Account.class);
                updateUI(account);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "loadAccount:onCancelled", databaseError.toException());
            }
        };
        account_ref.child(uid).addValueEventListener(accountListener);
    }

    private void updateUI(Account account) {
        tvName.setText(account.getName());
        tvUsername.setText(account.getUsername());
        tvEmail.setText(globalUser.getEmail());
        tvGender.setText(account.getGender());
        tvAddress.setText(account.getAddress());
        tvPhone.setText(account.getPhone_mo());
        tvBirthDay.setText(account.getBirth_day());
    }

    private void addEvent() {
        lnEditInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent edit_info_intent = new Intent(UserInfoActivity.this, EditInfoActivity.class);
                edit_info_intent.putExtra(USERNAME, account.getUsername());
                edit_info_intent.putExtra(NAME, account.getName());
                edit_info_intent.putExtra(ADDRESS, account.getAddress());
                edit_info_intent.putExtra(PHONE, account.getPhone_mo());
                edit_info_intent.putExtra(BIRTHDAY, account.getBirth_day());
                edit_info_intent.putExtra(GENDER, account.getGender());
                startActivity(edit_info_intent);
            }
        });

        lnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent change_password_intent = new Intent(UserInfoActivity.this, ChangePasswordActivity.class);
                startActivity(change_password_intent);
            }
        });

        lnContenManagement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent management_intent = new Intent(UserInfoActivity.this, ContentManagementActivity.class);
                startActivity(management_intent);
            }
        });

        lnSignout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db_helper.signOut();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home_intent = new Intent(UserInfoActivity.this, HomeActivity.class);
                startActivity(home_intent);
                finish();
            }
        });
    }
}
