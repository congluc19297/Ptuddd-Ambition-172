package com.ambition.moviereview.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by nhan on 05/05/2018.
 */

public class Movie {
    // Attributes
    private String id = "";
    private String name = "";
    // List of genres of this film
    private ArrayList<String> genres = new ArrayList<>();
    // List of actors who joined this film
    private ArrayList<String> actors = new ArrayList<>();
    // List of director who producted this film
    private ArrayList<String> director = new ArrayList<>();
    private String release_date = "";
    // Max rating is 5
    private float avg_rating = 5.0f;
    private String country = "";
    private String duration = "0";
    private String production_company = "";
    private String content = "";
    private String imageURL = "";
    private String owner_uid = "";
    private ArrayList<Comment> comments = new ArrayList<>();

    public Movie() {
    }

    public Movie(String name) {
        this.name = name;
    }

    public Movie(String id, String name, ArrayList<String> genres,
                 ArrayList<String> actors, ArrayList<String> director,
                 String release_date, float avg_rating, String country,
                 String duration, String production_company,
                 String content, String imageURL, String owner_uid, ArrayList<Comment> comments) {
        this.id = id;
        this.name = name;
        this.genres = genres;
        this.actors = actors;
        this.director = director;
        this.release_date = release_date;
        this.avg_rating = avg_rating;
        this.country = country;
        this.duration = duration;
        this.production_company = production_company;
        this.content = content;
        this.imageURL = imageURL;
        this.owner_uid = owner_uid;
        this.comments = comments;
    }

    public Movie(String name, ArrayList<String> genres, ArrayList<String> actors, ArrayList<String> director,
                 String release_date, float avg_rating, String country, String duration, String production_company,
                 String content, String imageURL, ArrayList<Comment> comments, String owner_uid) {
        this.name = name;
        this.genres = genres;
        this.actors = actors;
        this.director = director;
        this.release_date = release_date;
        this.avg_rating = avg_rating;
        this.country = country;
        this.duration = duration;
        this.production_company = production_company;
        this.content = content;
        this.imageURL = imageURL;
        this.comments = comments;
        this.owner_uid = owner_uid;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner_uid() {
        return owner_uid;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    public void setOwner_uid(String owner_uid) {
        this.owner_uid = owner_uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<String> genres) {
        this.genres = genres;
    }

    public ArrayList<String> getActors() {
        return actors;
    }

    public void setActors(ArrayList<String> actors) {
        this.actors = actors;
    }

    public ArrayList<String> getDirector() {
        return director;
    }

    public void setDirector(ArrayList<String> director) {
        this.director = director;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public float getAvg_rating() {
        return avg_rating;
    }

    public void setAvg_rating(float avg_rating) {
        this.avg_rating = avg_rating;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getProduction_company() {
        return production_company;
    }

    public void setProduction_company(String production_company) {
        this.production_company = production_company;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
