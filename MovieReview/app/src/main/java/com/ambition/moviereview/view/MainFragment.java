package com.ambition.moviereview.view;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ambition.moviereview.R;
import com.ambition.moviereview.controller.MovieAdapter;
import com.ambition.moviereview.model.Movie;
import com.ambition.moviereview.utilities.ItemClickListener;

import java.util.ArrayList;

import static com.ambition.moviereview.utilities.Constants.MOVIE_ID;

public class MainFragment extends Fragment {
    private final String TAG = MainFragment.class.getSimpleName();
    private ArrayList<Movie> data = new ArrayList<>();
    RecyclerView rvMovies;
    SwipeRefreshLayout refreshLayout;
    MovieAdapter adapter;
    public MainFragment() {
    }

    @SuppressLint("ValidFragment")
    public MainFragment(ArrayList<Movie> data) {
        this.data = data;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        // Lookup the recyclerview in activity layout
        rvMovies = (RecyclerView) view.findViewById(R.id.rcl_movies);

        // refresh layout
        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        // set color of loading indicator
        refreshLayout.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.colorAccent));
        // Initialize movies
        // Create adapter for data
        adapter = new MovieAdapter(data, getContext(), new ItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                String movieId = data.get(position).getId();
                Log.e(TAG, "movie_id: " + movieId);
                Intent detail_intent = new Intent(getContext(), MovieDetailActivity.class);
                detail_intent.putExtra(MOVIE_ID, movieId);
                getContext().startActivity(detail_intent);
            }
        });
        // Set fixed size to have smooth scroll
        rvMovies.hasFixedSize();
        // Attach the adapter to the recyclerview to populate items
        rvMovies.setAdapter(adapter);
        // Set layout manager to position the items
        rvMovies.setLayoutManager(new LinearLayoutManager(getContext()));

        // event when user swipe a screen then start refreshing the dataset of rclview
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adapter.notifyDataSetChanged();
                refreshLayout.setRefreshing(false);
            }
        });
        // That's all!
        return view;
    }
}
