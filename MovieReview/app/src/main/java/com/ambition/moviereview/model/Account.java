package com.ambition.moviereview.model;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by nhan on 05/05/2018.
 */

public class Account {
    // Attributes
    private String username = "";
    private String name = "";
    private String email = "";
    private String phone_mo = "";
    private String birth_day = "01/01/1970";
    private String address = "";
    private String gender = "";

    public Account(String username, String name, String email, String phone_mo, String birth_day, String address, String gender) {
        this.username = username;
        this.name = name;
        this.email = email;
        this.phone_mo = phone_mo;
        this.birth_day = birth_day;
        this.address = address;
        this.gender = gender;
    }

    public Account() {

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_mo() {
        return phone_mo;
    }

    public void setPhone_mo(String phone_mo) {
        this.phone_mo = phone_mo;
    }

    public String getBirth_day() {
        return birth_day;
    }

    public void setBirth_day(String birth_day) {
        this.birth_day = birth_day;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
