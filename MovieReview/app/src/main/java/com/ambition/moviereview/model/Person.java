package com.ambition.moviereview.model;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by nhan on 05/05/2018.
 */

public class Person {
    // Attributes
    private String id = "";
    private String name = "";
    private String birth_day = "01-01-1970";
    private String gender = "";
    private String nationality = "";
    private String info = "";
    private String type = "";
    private String owner_uid = "";

    public Person() {
    }

    public Person(String id, String name, String birth_day, String gender, String nationality, String info, String type, String uid) {
        this.id = id;
        this.name = name;
        this.birth_day = birth_day;
        this.gender = gender;
        this.nationality = nationality;
        this.info = info;
        this.type = type;
        this.owner_uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirth_day() {
        return birth_day;
    }

    public void setBirth_day(String birth_day) {
        this.birth_day = birth_day;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOwner_uid() {
        return owner_uid;
    }

    public void setOwner_uid(String owner_uid) {
        this.owner_uid = owner_uid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
