package com.ambition.moviereview.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ambition.moviereview.R;
import com.ambition.moviereview.utilities.ResponsiveImageView;
import com.balysv.materialripple.MaterialRippleLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static com.ambition.moviereview.utilities.Constants.ACTOR;
import static com.ambition.moviereview.utilities.Constants.DIRECTOR;
import static com.ambition.moviereview.utilities.Constants.KIND;
import static com.ambition.moviereview.utilities.Constants.MOVIE;
import static com.ambition.moviereview.utilities.Constants.UID;
import static com.ambition.moviereview.utilities.Constants.USER_LOGGED;

public class BaseActivity extends AppCompatActivity {
    private final String TAG = BaseActivity.class.getSimpleName();

    protected MaterialRippleLayout btnSignIn;
    protected MaterialRippleLayout btnSearch;
    protected MaterialRippleLayout btnAdd;
    protected ImageView imgHeader;
    protected static FirebaseUser globalUser = FirebaseAuth.getInstance().getCurrentUser();
    protected PopupMenu popup;
    protected boolean isShow = false;
    protected ResponsiveImageView imgAdd;
    protected RelativeLayout root;
    protected Context context;
    protected CharSequence[] generes = {"Hành động", "Hài", "Kinh dị", "Cổ trang", "Hoạt hình",
            "Võ thuật", "Phiêu lưu"
    };

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference movie_ref = database.getReference(MOVIE);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        context = this;
        matchHeaderId();
        setupHeader(false);
    }


    public ProgressDialog mProgressDialog;

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Đang xử lí");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    protected void matchHeaderId() {
        btnSignIn = (MaterialRippleLayout) findViewById(R.id.btn_sign_in);
        btnSearch = (MaterialRippleLayout) findViewById(R.id.btn_search);
        btnAdd = (MaterialRippleLayout) findViewById(R.id.btn_add);
        imgHeader = (ImageView) findViewById(R.id.img_left);
        imgAdd = (ResponsiveImageView) findViewById(R.id.img_add);
        root = (RelativeLayout) findViewById(R.id.root);
    }

    protected void setupHeader(boolean is_logged) {
        if (!is_logged) {
            imgHeader.setImageResource(R.drawable.ic_sign_in);
            btnSignIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goSignIn();
                }
            });
            btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goSignIn();
                }
            });
        } else {
            imgHeader.setImageResource(R.drawable.ic_user);
            btnSignIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent user_info_intent = new Intent(BaseActivity.this, UserInfoActivity.class);
                    startActivity(user_info_intent);
                }
            });
            btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPopupMenu();
                }
            });
        }
    }

    public void goSignIn() {
        Intent signin_intent = new Intent(BaseActivity.this, SignInActivity.class);
        startActivity(signin_intent);
    }

    public static void autoHideKeyboard(View view, final Activity activity) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new EditText.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(activity);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                autoHideKeyboard(innerView, activity);
            }
        }
    }


    private static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public boolean isLogged() {
        if (globalUser != null) {
            return true;
        } else {
            return false;
        }
    }

    public void showPopupMenu() {
        isShow = true;
        //Creating the instance of PopupMenu
        popup = new PopupMenu(BaseActivity.this, btnAdd);
        //Inflating the Popup using xml file
        popup.getMenuInflater()
                .inflate(R.menu.add_menu, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.btn_add_movie:
                        // Add movie
                        Intent add_movie_intent = new Intent(context, AddMovieActivity.class);
                        startActivity(add_movie_intent);
                        break;
                    case R.id.btn_add_actor:
                        // Add actor
                        Intent add_actor_intent = new Intent(context, AddPersonActivity.class);
                        add_actor_intent.putExtra(KIND, ACTOR);
                        startActivity(add_actor_intent);
                        break;
                    case R.id.add_director:
                        // Add director
                        Intent add_director_intent = new Intent(context, AddPersonActivity.class);
                        add_director_intent.putExtra(KIND, DIRECTOR);
                        startActivity(add_director_intent);
                        break;
                    default:
                        break;
                }
                return true;
            }
        });  //closing the setOnClickListener method
        popup.show(); //showing popup menu
    }

    public void savingLogging(String uid) {
        SharedPreferences.Editor editor = getSharedPreferences(USER_LOGGED, MODE_PRIVATE).edit();
        editor.putString(UID, uid);
        editor.apply();
    }

    public void retrieveData() {
        SharedPreferences prefs = getSharedPreferences(USER_LOGGED, MODE_PRIVATE);
        String uid = prefs.getString(UID, null);
        if (uid !=null) {
            // If have uid that mean user have logged before
            // Get account info from firebase
        }
    }

}
