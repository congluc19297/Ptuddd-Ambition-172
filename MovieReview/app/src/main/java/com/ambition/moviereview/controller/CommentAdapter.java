package com.ambition.moviereview.controller;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ambition.moviereview.R;
import com.ambition.moviereview.model.Comment;

import java.util.ArrayList;

import static com.ambition.moviereview.utilities.Utilities.getTimeFromMilisecond;

/**
 * Created by nhan on 18/05/2018.
 */

//public class CommentAdapter extends ArrayAdapter<Comment> {
//
//    public CommentAdapter(Context context, ArrayList<Comment> data) {
//        super(context, 0, data);
//    }
//
//    @NonNull
//    @Override
//    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
//        // Get the data item for this position
//        Comment comment = getItem(position);
//        // Check if an existing view is being reused, otherwise inflate the view
//        if (convertView == null) {
//            convertView = LayoutInflater.from(getContext()).inflate(R.layout.comment_item, parent, false);
//        }
//        // Link view by id
//        TextView tvUsername = (TextView) convertView.findViewById(R.id.tv_username);
//        TextView tvContent = (TextView) convertView.findViewById(R.id.tv_content);
//        TextView tvTime = (TextView) convertView.findViewById(R.id.tv_time);
//        ImageView mStar1 = (ImageView) convertView.findViewById(R.id.img_s1);
//        ImageView mStar2 = (ImageView) convertView.findViewById(R.id.img_s2);
//        ImageView mStar3 = (ImageView) convertView.findViewById(R.id.img_s3);
//        ImageView mStar4 = (ImageView) convertView.findViewById(R.id.img_s4);
//        ImageView mStar5 = (ImageView) convertView.findViewById(R.id.img_s5);
//
//        // Update data
//        tvUsername.setText(comment.getUser().getName());
//        tvContent.setText(comment.getComment_content());
//        String time = getTimeFromMilisecond(comment.getTime_comment(), "dd/MM/yyyy hh:mm");
//        tvTime.setText(time);
//        int rating = comment.getRating_level();
//        switch (rating) {
//            case 1:
//                mStar2.setImageResource(R.drawable.ic_empty);
//                mStar3.setImageResource(R.drawable.ic_empty);
//                mStar4.setImageResource(R.drawable.ic_empty);
//                mStar5.setImageResource(R.drawable.ic_empty);
//                break;
//            case 2:
//                mStar3.setImageResource(R.drawable.ic_empty);
//                mStar4.setImageResource(R.drawable.ic_empty);
//                mStar5.setImageResource(R.drawable.ic_empty);
//                break;
//            case 3:
//                mStar4.setImageResource(R.drawable.ic_empty);
//                mStar5.setImageResource(R.drawable.ic_empty);
//                break;
//            case 4:
//                mStar5.setImageResource(R.drawable.ic_empty);
//                break;
//            case 5:
//                break;
//            default:
//                break;
//        }
//        return convertView;
//    }
//
//
//}

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {

    private ArrayList<Comment> mComments;

    // Pass in the contact array into the constructor
    public CommentAdapter(ArrayList<Comment> comments) {
        mComments = comments;
    }
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        TextView mUserName;
        TextView mContent;
        TextView mTime;
        ImageView mS1;
        ImageView mS2;
        ImageView mS3;
        ImageView mS4;
        ImageView mS5;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

        mUserName = (TextView) itemView.findViewById(R.id.tv_username);
        mContent = (TextView) itemView.findViewById(R.id.tv_content);
        mTime = (TextView) itemView.findViewById(R.id.tv_time);
        mS1 = (ImageView) itemView.findViewById(R.id.img_s1);
        mS2 = (ImageView) itemView.findViewById(R.id.img_s2);
        mS3 = (ImageView) itemView.findViewById(R.id.img_s3);
        mS4 = (ImageView) itemView.findViewById(R.id.img_s4);
        mS5 = (ImageView) itemView.findViewById(R.id.img_s5);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.comment_item, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(CommentAdapter.ViewHolder viewHolder, int position) {
        // Get the data model based on position
        Comment comment = mComments.get(position);

        // Set item views based on your views and data model
        TextView tvUsername = viewHolder.mUserName;
        TextView tvContent = viewHolder.mContent;
        TextView tvTime = viewHolder.mTime;
        ImageView mStar1 = viewHolder.mS1;
        ImageView mStar2 = viewHolder.mS2;
        ImageView mStar3 = viewHolder.mS3;
        ImageView mStar4 = viewHolder.mS4;
        ImageView mStar5 = viewHolder.mS5;

        // Update data
        tvUsername.setText(comment.getUser().getName());
        tvContent.setText(comment.getComment_content());
        String time = getTimeFromMilisecond(comment.getTime_comment(), "dd/MM/yyyy hh:mm");
        tvTime.setText(time);
        int rating = comment.getRating_level();
        switch (rating) {
            case 1:
                mStar2.setImageResource(R.drawable.ic_empty);
                mStar3.setImageResource(R.drawable.ic_empty);
                mStar4.setImageResource(R.drawable.ic_empty);
                mStar5.setImageResource(R.drawable.ic_empty);
                break;
            case 2:
                mStar3.setImageResource(R.drawable.ic_empty);
                mStar4.setImageResource(R.drawable.ic_empty);
                mStar5.setImageResource(R.drawable.ic_empty);
                break;
            case 3:
                mStar4.setImageResource(R.drawable.ic_empty);
                mStar5.setImageResource(R.drawable.ic_empty);
                break;
            case 4:
                mStar5.setImageResource(R.drawable.ic_empty);
                break;
            case 5:
                break;
            default:
                break;
        }

    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return mComments.size();
    }
}
