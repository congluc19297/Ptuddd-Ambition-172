package com.ambition.moviereview.controller;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ambition.moviereview.R;
import com.ambition.moviereview.model.Comment;
import com.ambition.moviereview.model.Movie;

import java.util.ArrayList;

import static com.ambition.moviereview.utilities.Utilities.getTimeFromMilisecond;

/**
 * Created by nhan on 20/05/2018.
 */

public class MovieContentAdapter extends ArrayAdapter<Movie> {

    public MovieContentAdapter(Context context, ArrayList<Movie> data) {
        super(context, 0, data);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // Get the data item for this position
        Movie movie = getItem(position);
        Log.e("adapter", movie.getName());
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.content_item, parent, false);
        }
        // Link view by id
        TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
        TextView tvContent = (TextView) convertView.findViewById(R.id.tv_content);


        // Update data
        tvTitle.setText(movie.getName());
        tvContent.setText(movie.getProduction_company());

        return convertView;
    }
}
