package com.ambition.moviereview.utilities;

/**
 * Created by nhan on 06/05/2018.
 */

public class Constants {
    public static final String MOVIE = "movie";
    public static final String ACCOUNT = "account";
    public static final String COMMENT = "comment";
    public static final String PERSON = "person";
    public static final String GENERES = "genere";
    public static final String USERNAME = "username";
    public static final String NAME = "name";
    public static final String ADDRESS = "address";
    public static final String PHONE = "phone";
    public static final String BIRTHDAY = "birthday";
    public static final String GENDER = "gender";
    public static final String MALE = "Nam";
    public static final String FEMALE = "Nữ";
    public static final String OTHER = "Khác";
    public static final String KIND = "person_kind";
    public static final String ACTOR = "actor";
    public static final String DIRECTOR = "director";
    public static final String MOVIE_ID = "movie_id";
    public static final String UID = "uid";
    public static final String USER_LOGGED = "user_logged";
    public static final String IS_EDIT = "is_edit";
    public static final String PERSON_ID = "person_id";
}
