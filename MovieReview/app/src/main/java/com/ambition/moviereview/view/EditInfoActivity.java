package com.ambition.moviereview.view;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ambition.moviereview.R;
import com.ambition.moviereview.controller.FireBaseHelper;
import com.ambition.moviereview.model.Account;
import com.balysv.materialripple.MaterialRippleLayout;

import static com.ambition.moviereview.utilities.Constants.ADDRESS;
import static com.ambition.moviereview.utilities.Constants.BIRTHDAY;
import static com.ambition.moviereview.utilities.Constants.FEMALE;
import static com.ambition.moviereview.utilities.Constants.GENDER;
import static com.ambition.moviereview.utilities.Constants.MALE;
import static com.ambition.moviereview.utilities.Constants.NAME;
import static com.ambition.moviereview.utilities.Constants.PHONE;
import static com.ambition.moviereview.utilities.Constants.USERNAME;
import static com.ambition.moviereview.utilities.Utilities.checkUsername;

public class EditInfoActivity extends BaseActivity {
    private final String TAG = SignUpActivity.class.getSimpleName();
    // Views in layout
    private EditText edtUsername, edtAddress, edtPhone, edtName;
    private TextView tvBirthday;
    private Button btnPickDate, btnSubmit;
    private MaterialRippleLayout btnBack, btnHome;
    private RadioGroup rdgGender;
    private RadioButton rdbMale, rdbFemale, rdbOther;
    private String username = "", address = "", phone = "", birthday = "", gender = "", name = "";
    // Database controller
    private FireBaseHelper db_helper;
    private Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_info);

        // My function
        autoHideKeyboard(findViewById(R.id.root), this);
        db_helper = new FireBaseHelper(this);
        intent = getIntent();
        extractData();
        matchViewsId();
        initDataToView();
        setEvent();

        // Change font to Roboto
        pickDate();
        submit();
    }

    private void extractData() {
        username = intent.getStringExtra(USERNAME);
        name = intent.getStringExtra(NAME);
        address = intent.getStringExtra(ADDRESS);
        phone = intent.getStringExtra(PHONE);
        birthday = intent.getStringExtra(BIRTHDAY);
        gender = intent.getStringExtra(GENDER);
    }

    private void initDataToView() {
        edtUsername.setText(username);
        edtName.setText(name);
        edtAddress.setText(address);
        edtPhone.setText(phone);
        tvBirthday.setText(birthday);
        switch (birthday) {
            case MALE:
                rdbMale.setChecked(true);
                break;
            case FEMALE:
                rdbFemale.setChecked(true);
                break;
            default:
                rdbOther.setChecked(true);
                break;
        }
    }

    private void matchViewsId() {
        edtUsername = (EditText) findViewById(R.id.edt_username);
        edtAddress = (EditText) findViewById(R.id.edt_address);
        edtPhone = (EditText) findViewById(R.id.edt_phone);
        edtName = (EditText) findViewById(R.id.edt_name);
        tvBirthday = (TextView) findViewById(R.id.tv_birthday);
        btnPickDate = (Button) findViewById(R.id.btn_pickdate);
        btnSubmit = (Button) findViewById(R.id.btn_submit);
        btnBack = (MaterialRippleLayout) findViewById(R.id.btn_back);
        btnHome = (MaterialRippleLayout) findViewById(R.id.btn_home);
        rdgGender = (RadioGroup) findViewById(R.id.rdg_gender);
        rdbMale = (RadioButton) findViewById(R.id.rdt_male);
        rdbFemale = (RadioButton) findViewById(R.id.rdt_female);
        rdbOther = (RadioButton) findViewById(R.id.rdt_other);
    }

    private void setEvent() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home_intent = new Intent(EditInfoActivity.this, HomeActivity.class);
                startActivity(home_intent);
                finish();
            }
        });
    }


    private void pickDate() {
        btnPickDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });
    }

    private void showDatePicker() {
        DatePickerDialog.OnDateSetListener callback = new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear,
                                  int dayOfMonth) {
                // Update textview by date pick
                tvBirthday.setText(
                        (dayOfMonth) + "/" + (monthOfYear + 1) + "/" + year);
            }
        };
        // Set day in DatePicker match with textview
        String s = tvBirthday.getText().toString();
        String strArrtmp[] = s.split("/");
        int day = Integer.parseInt(strArrtmp[0]);
        int month = Integer.parseInt(strArrtmp[1]) - 1;
        int year = Integer.parseInt(strArrtmp[2]);
        DatePickerDialog pic = new DatePickerDialog(
                this,
                callback, year, month, day);
        pic.setTitle("Chọn ngày sinh");
        pic.show();
    }

    private boolean validateData() {
        username = edtUsername.getText().toString();
        address = edtAddress.getText().toString();
        phone = edtPhone.getText().toString();
        name = edtName.getText().toString();
        if (username.isEmpty() || address.isEmpty() || phone.isEmpty() || name.isEmpty()) {
            Toast.makeText(this, "Vui lòng điền đầy đủ các trường", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!checkUsername(username)) {
            Toast.makeText(this, "Tên đăng nhập sai định dạng", Toast.LENGTH_SHORT).show();
            return  false;
        }
        return true;
    }

    private void getGender() {
        int checkedId = rdgGender.getCheckedRadioButtonId();
        switch (checkedId) {
            case R.id.rdt_male:
                gender = "Nam";
                break;
            case R.id.rdt_female:
                gender = "Nữ";
                break;
            default:
                gender = "Khác";
                break;
        }
    }

    private void getBirthday() {
        birthday = tvBirthday.getText().toString();
    }

    private boolean submitRegisteration() {
        if (validateData()) {
            getGender();
            getBirthday();
            String email = globalUser.getEmail();
            String uid = globalUser.getUid();
            Account newAccount = new Account(username, name, email, phone, birthday, address, gender);
            Log.e(TAG, "username: " + username);
            Log.e(TAG, "email " + email);
            db_helper.addAccount(newAccount, uid);
            Toast.makeText(this, "Thay đổi thông tin thành công", Toast.LENGTH_SHORT).show();
            Intent edit_info = new Intent(EditInfoActivity.this, UserInfoActivity.class);
            startActivity(edit_info);
            finish();

        } else {
            return false;
        }
        return true;
    }

    private void submit() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitRegisteration();
            }
        });
    }
}