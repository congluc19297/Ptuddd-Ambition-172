package com.ambition.moviereview.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ambition.moviereview.R;
import com.ambition.moviereview.controller.FireBaseHelper;
import com.balysv.materialripple.MaterialRippleLayout;

public class ChangePasswordActivity extends BaseActivity {
    private MaterialRippleLayout btnBack, btnHome;
    private EditText edtOldPass, edtNewPass, edtReNewPass;
    private Button btnSubmit;

    private String oldPass ="", newPass = "", reNewPass = "";
    private FireBaseHelper db_helper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        db_helper = new FireBaseHelper(this);
        // My functions
        matchViewsId();
        setEvent();
        submit();
    }

    private void matchViewsId() {
        btnBack = (MaterialRippleLayout) findViewById(R.id.btn_back);
        btnHome = (MaterialRippleLayout) findViewById(R.id.btn_home);
        edtOldPass = (EditText) findViewById(R.id.edt_old_password);
        edtNewPass = (EditText) findViewById(R.id.edt_new_password);
        edtReNewPass = (EditText) findViewById(R.id.edt_repassword);
        btnSubmit = (Button) findViewById(R.id.btn_submit);
    }

    private void setEvent() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home_intent = new Intent(ChangePasswordActivity.this, HomeActivity.class);
                startActivity(home_intent);
                finish();
            }
        });
    }

    private void submit() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateData()) {
                    db_helper.changePassword(globalUser.getEmail(), oldPass, newPass);
                }
            }
        });
    }

    private boolean validateData() {
        oldPass = edtOldPass.getText().toString();
        newPass = edtNewPass.getText().toString();
        reNewPass = edtReNewPass.getText().toString();
        if (newPass.length() < 5) {
            Toast.makeText(this, "Mật khẩu quá ngắn", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (!newPass.equals(reNewPass)) {
            Toast.makeText(this, "Mật khẩu không khớp", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (oldPass.equals(newPass)) {
            Toast.makeText(this, "Mật khẩu mới trùng mật khẩu cũ", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
