package com.ambition.moviereview.utilities;

/**
 * Created by nhan on 05/05/2018.
 */

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.support.v7.widget.AppCompatImageView;

public class ResponsiveImageView extends AppCompatImageView {

    public ResponsiveImageView(Context context) {
        super(context);
    }

    public ResponsiveImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ResponsiveImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Drawable d = getDrawable();
        if (d != null) {
            int w = MeasureSpec.getSize(widthMeasureSpec);
            int h = w * d.getIntrinsicHeight() / d.getIntrinsicWidth();
            setMeasuredDimension(w, h);
        }
        else super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}