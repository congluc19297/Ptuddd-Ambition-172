package com.ambition.moviereview.view;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ambition.moviereview.R;
import com.ambition.moviereview.controller.FireBaseHelper;
import com.ambition.moviereview.model.Account;
import com.balysv.materialripple.MaterialRippleLayout;

import java.util.Calendar;
import java.util.Date;

import static com.ambition.moviereview.utilities.Constants.FEMALE;
import static com.ambition.moviereview.utilities.Constants.MALE;
import static com.ambition.moviereview.utilities.Constants.OTHER;
import static com.ambition.moviereview.utilities.Utilities.checkEmail;
import static com.ambition.moviereview.utilities.Utilities.checkUsername;
import static com.ambition.moviereview.utilities.Utilities.getDateFormat;

public class SignUpActivity extends BaseActivity {
    private final String TAG = SignUpActivity.class.getSimpleName();
    // Views in layout
    private EditText edtEmail, edtUsername, edtPassword, edtRePassword, edtAddress, edtPhone, edtName;
    private TextView tvBirthday;
    private Button btnPickDate, btnSubmit;
    private MaterialRippleLayout btnBack, btnHome;
    private RadioButton rdbMale, rdbFemale, rdbOther;
    private RadioGroup rdgGender;
    private String email = "", username = "", password = "", repassword = "",
            address = "", phone = "", birthday = "", gender = "", name = "";
    // Database controller
    private FireBaseHelper db_helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        // My function
        autoHideKeyboard(findViewById(R.id.root), this);
        db_helper = new FireBaseHelper(this);
        matchViewsId();
        setEvent();
        // Change font to Roboto
        setDefaultDay();
        pickDate();
        submit();
    }

    private void matchViewsId() {
        edtEmail = (EditText) findViewById(R.id.edt_email);
        edtUsername = (EditText) findViewById(R.id.edt_username);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        edtRePassword = (EditText) findViewById(R.id.edt_repassword);
        edtAddress = (EditText) findViewById(R.id.edt_address);
        edtPhone = (EditText) findViewById(R.id.edt_phone);
        edtName = (EditText) findViewById(R.id.edt_name);
        tvBirthday = (TextView) findViewById(R.id.tv_birthday);
        btnPickDate = (Button) findViewById(R.id.btn_pickdate);
        btnSubmit = (Button) findViewById(R.id.btn_submit);
        btnBack = (MaterialRippleLayout) findViewById(R.id.btn_back);
        btnHome = (MaterialRippleLayout) findViewById(R.id.btn_home);
        rdbMale = (RadioButton) findViewById(R.id.rdt_male);
        rdbFemale = (RadioButton) findViewById(R.id.rdt_female);
        rdbOther = (RadioButton) findViewById(R.id.rdt_other);
        rdgGender = (RadioGroup) findViewById(R.id.rdg_gender);
    }

    private void setEvent() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home_intent = new Intent(SignUpActivity.this, HomeActivity.class);
                startActivity(home_intent);
                finish();
            }
        });
    }

    private void setDefaultDay() {
        Date date = Calendar.getInstance().getTime();
        tvBirthday.setText(getDateFormat(date));
    }

    private void pickDate() {
        btnPickDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });
    }

    private void showDatePicker() {
        DatePickerDialog.OnDateSetListener callback = new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear,
                                  int dayOfMonth) {
                // Update textview by date pick
                tvBirthday.setText(
                        (dayOfMonth) + "/" + (monthOfYear + 1) + "/" + year);
            }
        };
        // Set day in DatePicker match with textview
        String s = tvBirthday.getText().toString();
        String strArrtmp[] = s.split("/");
        int day = Integer.parseInt(strArrtmp[0]);
        int month = Integer.parseInt(strArrtmp[1]) - 1;
        int year = Integer.parseInt(strArrtmp[2]);
        DatePickerDialog pic = new DatePickerDialog(
                this,
                callback, year, month, day);
        pic.setTitle("Chọn ngày sinh");
        pic.show();
    }

    private boolean validateData() {
        email = edtEmail.getText().toString();
        username = edtUsername.getText().toString();
        password = edtPassword.getText().toString();
        repassword = edtRePassword.getText().toString();
        address = edtAddress.getText().toString();
        phone = edtPhone.getText().toString();
        name = edtName.getText().toString();
        Log.e(TAG, "password: " + password);
        Log.e(TAG, "repassword: " + repassword);
        if (email.isEmpty() || username.isEmpty() || password.isEmpty()
                || repassword.isEmpty() || address.isEmpty() || phone.isEmpty() || name.isEmpty()) {
            Toast.makeText(this, "Vui lòng điền đầy đủ các trường", Toast.LENGTH_SHORT).show();
            return false;
        } else if (password.length() < 5) {
            edtPassword.requestFocus();
            Toast.makeText(this, "Mật khẩu quá ngắn, cần tối thiểu 4 ký tự", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!password.equals(repassword)) {
            edtRePassword.requestFocus();
            Toast.makeText(this, "Nhập lại mật khẩu sai", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!checkEmail(email)) {
            Toast.makeText(this, "Email sai định dạng", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!checkUsername(username)) {
            Toast.makeText(this, "Tên đăng nhập sai định dạng", Toast.LENGTH_SHORT).show();
            return  false;
        }
        return true;
    }

    private void getGender() {
        int checkedId = rdgGender.getCheckedRadioButtonId();
        switch (checkedId) {
            case R.id.rdt_male:
                gender = MALE;
                break;
            case R.id.rdt_female:
                gender = FEMALE;
                break;
            default:
                gender = OTHER;
                break;
        }
    }

    private void getBirthday() {
        birthday = tvBirthday.getText().toString();
    }

    private boolean submitRegisteration() {
        if (validateData()) {
            getGender();
            getBirthday();
            Account newAccount = new Account(username, name, email, phone, birthday, address, gender);
            Log.e(TAG, "username: " + username);
            Log.e(TAG, "email " + email);
            db_helper.addNewAccount(email, password, newAccount);
        } else {
            return false;
        }
        return true;
    }

    private void submit() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitRegisteration();
            }
        });
    }
}