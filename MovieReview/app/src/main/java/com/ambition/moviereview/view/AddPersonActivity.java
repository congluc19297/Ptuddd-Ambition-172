package com.ambition.moviereview.view;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ambition.moviereview.R;
import com.ambition.moviereview.controller.FireBaseHelper;
import com.ambition.moviereview.model.Person;
import com.balysv.materialripple.MaterialRippleLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.Date;

import static com.ambition.moviereview.utilities.Constants.ACTOR;
import static com.ambition.moviereview.utilities.Constants.DIRECTOR;
import static com.ambition.moviereview.utilities.Constants.FEMALE;
import static com.ambition.moviereview.utilities.Constants.IS_EDIT;
import static com.ambition.moviereview.utilities.Constants.KIND;
import static com.ambition.moviereview.utilities.Constants.MALE;
import static com.ambition.moviereview.utilities.Constants.OTHER;
import static com.ambition.moviereview.utilities.Constants.PERSON_ID;
import static com.ambition.moviereview.utilities.Utilities.getDateFormat;

public class AddPersonActivity extends BaseActivity {
    private final String TAG = AddPersonActivity.class.getSimpleName();
    private TextView tvBirthday, tvTitle;
    private EditText edtName, edtNation, edtInfo;
    private Button btnPickDate, btnSubmit;
    private MaterialRippleLayout btnBack, btnHome;
    private RadioGroup rdgGender;
    private RadioButton rdbMale, rdbFemale, rdbOther;
    private String birthday = "", gender = "", name = "", nationality = "", info = "";

    // Database controller
    private FireBaseHelper db_helper;
    private final FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference account_ref;
    private Person account = new Person();

    private Intent intent;
    private String personType = "";
    private boolean isEdit = false;
    private String personId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_person);

        // My functions
        intent = getIntent();
        personType = intent.getStringExtra(KIND);
        isEdit = intent.getBooleanExtra(IS_EDIT, false);
        personId = intent.getStringExtra(PERSON_ID);
        Log.e(TAG, isEdit + personId);
        // Hard code value for type
        // personType = DIRECTOR;
        db_helper = new FireBaseHelper(this);
        autoHideKeyboard(findViewById(R.id.root), this);
        matchViewId();
        setEvent();
        // Change font to Roboto
        setDefaultDay();
        pickDate();
        if (personType.equals(DIRECTOR)) {
            account_ref = database.getReference(DIRECTOR);
            tvTitle.setText(getString(R.string.add_director));
        } else {
            account_ref = database.getReference(ACTOR);
            tvTitle.setText(getString(R.string.add_actor));
        }
        if (isEdit && !personId.isEmpty()) {
            loadAccountInfo(personId);
        }
        submit();
    }

    private void loadAccountInfo(String personId) {
        ValueEventListener accountListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                account = dataSnapshot.getValue(Person.class);
                updateUI(account);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "loadAccount:onCancelled", databaseError.toException());
            }
        };
        account_ref.child(personId).addValueEventListener(accountListener);
    }

    private void updateUI(Person account) {
        tvBirthday.setText(account.getBirth_day());
        edtName.setText(account.getName());
        edtInfo.setText(account.getInfo());
        edtNation.setText(account.getNationality());
        if (account.getGender().equals(MALE)) {
            rdbMale.setChecked(true);
        } else if (account.getGender().equals(FEMALE)) {
            rdbFemale.setChecked(true);
        } else {
            rdbOther.setChecked(true);
        }
    }

    private void matchViewId() {
        tvBirthday = (TextView) findViewById(R.id.tv_birthday);
        btnBack = (MaterialRippleLayout) findViewById(R.id.btn_back);
        btnHome = (MaterialRippleLayout) findViewById(R.id.btn_home);
        btnPickDate = (Button) findViewById(R.id.btn_pickdate);
        btnSubmit = (Button) findViewById(R.id.btn_submit);
        edtName = (EditText) findViewById(R.id.edt_name);
        edtInfo = (EditText) findViewById(R.id.edt_info);
        edtNation = (EditText) findViewById(R.id.edt_nation);
        rdgGender = (RadioGroup) findViewById(R.id.rdg_gender);
        rdbFemale = (RadioButton) findViewById(R.id.rdt_female);
        rdbMale = (RadioButton) findViewById(R.id.rdt_male);
        rdbOther = (RadioButton) findViewById(R.id.rdt_other);
        tvTitle = (TextView) findViewById(R.id.tv_title);
    }

    private void setEvent() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home_intent = new Intent(AddPersonActivity.this, HomeActivity.class);
                startActivity(home_intent);
                finish();
            }
        });
    }

    private void setDefaultDay() {
        Date date = Calendar.getInstance().getTime();
        tvBirthday.setText(getDateFormat(date));
    }

    private void pickDate() {
        btnPickDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });
    }

    private void showDatePicker() {
        DatePickerDialog.OnDateSetListener callback = new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear,
                                  int dayOfMonth) {
                // Update textview by date pick
                tvBirthday.setText(
                        (dayOfMonth) + "/" + (monthOfYear + 1) + "/" + year);
            }
        };
        // Set day in DatePicker match with textview
        String s = tvBirthday.getText().toString();
        String strArrtmp[] = s.split("/");
        int day = Integer.parseInt(strArrtmp[0]);
        int month = Integer.parseInt(strArrtmp[1]) - 1;
        int year = Integer.parseInt(strArrtmp[2]);
        DatePickerDialog pic = new DatePickerDialog(
                this,
                callback, year, month, day);
        pic.setTitle("Chọn ngày sinh");
        pic.show();
    }

    private boolean validateData() {
        getGender();
        getBirthday();
        name = edtName.getText().toString();
        nationality = edtNation.getText().toString();
        info = edtInfo.getText().toString();
        if (name.isEmpty()) {
            Toast.makeText(this, "Thiếu họ và tên", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (nationality.isEmpty()) {
            Toast.makeText(this, "Thiếu quốc tịch", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void getGender() {
        int checkedId = rdgGender.getCheckedRadioButtonId();
        switch (checkedId) {
            case R.id.rdt_male:
                gender = MALE;
                break;
            case R.id.rdt_female:
                gender = FEMALE;
                break;
            default:
                gender = OTHER;
                break;
        }
    }

    private void goBackManagement() {
        Intent intent = new Intent(this, ContentManagementActivity.class);
        startActivity(intent);
    }

    private void getBirthday() {
        birthday = tvBirthday.getText().toString();
    }

    private boolean submitRegisteration() {
        if (validateData()) {
            if (personType.equals(DIRECTOR)) {
                Person newActor = new Person("",name, birthday, gender, nationality, info, personType, globalUser.getUid());
                if (!isEdit) {
                    db_helper.addDirector(newActor);
                    Toast.makeText(this, "Thêm đạo diễn thành công", Toast.LENGTH_SHORT).show();
                } else {
                    db_helper.updateDirector(account.getId(), newActor);
                    Toast.makeText(this, "Chỉnh sửa đạo diễn thành công", Toast.LENGTH_SHORT).show();
                    goBackManagement();
                }
            } else if (personType.equals(ACTOR)) {
                Person newDirector = new Person("",name, birthday, gender, nationality, info, personType, globalUser.getUid());
                if (!isEdit) {
                    db_helper.addActor(newDirector);
                    Toast.makeText(this, "Thêm diễn viên thành công", Toast.LENGTH_SHORT).show();
                } else {
                    db_helper.updateDirector(account.getId(), newDirector);
                    Toast.makeText(this, "Chỉnh sửa diễn viên thành công", Toast.LENGTH_SHORT).show();
                    goBackManagement();
                }
            }
            clearUI();
        }
        return true;
    }

    private void clearUI() {
        edtName.setText("");
        edtNation.setText("");
        edtInfo.setText("");
        setDefaultDay();
        edtName.requestFocus();
    }

    private void submit() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitRegisteration();
            }
        });
    }

}
