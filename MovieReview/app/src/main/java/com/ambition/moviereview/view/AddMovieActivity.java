package com.ambition.moviereview.view;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ambition.moviereview.R;
import com.ambition.moviereview.controller.FireBaseHelper;
import com.ambition.moviereview.model.Comment;
import com.ambition.moviereview.model.Movie;
import com.ambition.moviereview.model.Person;
import com.ambition.moviereview.utilities.ResponsiveImageView;
import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.ambition.moviereview.utilities.Constants.ACTOR;
import static com.ambition.moviereview.utilities.Constants.DIRECTOR;
import static com.ambition.moviereview.utilities.Constants.IS_EDIT;
import static com.ambition.moviereview.utilities.Constants.MOVIE;
import static com.ambition.moviereview.utilities.Constants.MOVIE_ID;
import static com.ambition.moviereview.utilities.Utilities.getDateFormat;

public class AddMovieActivity extends BaseActivity {
    private final String TAG = AddMovieActivity.class.getSimpleName();
    private EditText edtMovieName, edtManuafactor, edtDuration, edtInfo, edtCountry;
    private TextView tvActors, tvDirectors, tvGeneres, tvReleaseDay, tvTitle;
    private Button btnSelectActor, btnSelectDirector, btnSelectGenere, btnPickDate, btnSubmit;
    private ResponsiveImageView imgThumbnail;
    private MaterialRippleLayout btnBack, btnHome;
    private ImageView imgHolder;
    private RelativeLayout rltSelectThumbnail;

    // Pick up data
    private ArrayList<CharSequence> selectedGenere = new ArrayList<>();
    private ArrayList<CharSequence> selectedActor = new ArrayList<>();
    private ArrayList<CharSequence> selectedDirector = new ArrayList<>();
    private CharSequence[] actors = {};
    private CharSequence[] directors = {};
    private ArrayList<String> actorArray = new ArrayList<>();
    private ArrayList<String> directorArray = new ArrayList<>();
    private ArrayList<Comment> comments = new ArrayList<>();

    // Read data
    private FireBaseHelper db_helper;
    private final FirebaseDatabase database = FirebaseDatabase.getInstance();
    private final StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
    private final DatabaseReference actor_ref = database.getReference(ACTOR);
    private final DatabaseReference director_ref = database.getReference(DIRECTOR);

    private Person person = new Person();
    private Movie movie = new Movie();

    // Field in views map by variables
    private String movie_name = "", manuafactor = "", info = "", duration = "", img_link = "", release_day="", country = "";
    private static int IMG_RESULT = 1;
    private String ImageDecode;
    private Intent intent;
    private String[] FILE;
    private File imageFile;
    private boolean isEdit = false;
    private String movie_id = "";
    private boolean isChangeImage = false;

    DatabaseReference movie_ref = database.getReference(MOVIE);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_movie);

        // Load actor and director from cloud
        loadData();

        intent = getIntent();
        isEdit = intent.getBooleanExtra(IS_EDIT, false);
        movie_id = intent.getStringExtra(MOVIE_ID);

        db_helper = new FireBaseHelper(this);

        // My functions
        autoHideKeyboard(findViewById(R.id.root), this);
        matchViewId();
        setNavigationEvent();
        setEventWidget();
        setDefaultDay();
        pickDate();
        selectGeneres();
        selectActors();
        selectDirectors();

        if(isEdit && !movie_id.isEmpty()) {
            loadMovieData(movie_id);
        }
        if (isEdit) {
            tvTitle.setText("Chỉnh sửa phim");
            btnSubmit.setText("OK");
        }

        submit();
    }

    private void loadMovieData(String movie_id) {
        showProgressDialog();
        ValueEventListener movieListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e(TAG, movie.getName());
                movie = dataSnapshot.getValue(Movie.class);
                updateUI(movie);
                hideProgressDialog();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "loadMovie: failed - ", databaseError.toException());
            }
        };
        movie_ref.child(movie_id).addListenerForSingleValueEvent(movieListener);
    }

    private void updateUI(Movie movie) {
        edtMovieName.setText(movie.getName());
        edtInfo.setText(movie.getContent());
        edtManuafactor.setText(movie.getProduction_company());
        edtCountry.setText(movie.getCountry());
        edtDuration.setText(movie.getDuration());
        String genres = "";
        for(String genre: movie.getGenres()) {
            genres += genre + ", ";
            selectedGenere.add(genre);
        }
        tvGeneres.setText(genres);

        String directors = "";
        for(String director: movie.getDirector()) {
            directors += director + ", ";
            selectedDirector.add(director);
        }
        tvDirectors.setText(directors);

        String actors = "";
        for(String actor: movie.getActors()) {
            actors += actor + ", ";
            selectedActor.add(actor);
        }
        for(Comment comment: movie.getComments()) {
            comments.add(comment);
        }
        tvActors.setText(actors);
        tvReleaseDay.setText(movie.getRelease_date());
        Glide.with(this)
                .load(movie.getImageURL())
                .into(imgThumbnail);
    }

    private void setEventWidget() {
        rltSelectThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                browsePicture();
            }
        });
    }

    private void convertData() {
        actors = new CharSequence[actorArray.size()];
        directors = new CharSequence[directorArray.size()];
        for(int i = 0; i<actorArray.size(); i++) {
            actors[i] = actorArray.get(i);
        }
        for(int i = 0; i<directorArray.size(); i++) {
            directors[i] = directorArray.get(i);
        }
        for(int i = 0; i<directors.length; i++) {
            Log.e(TAG, directors[i].toString());
        }
    }

    private void loadData() {
        ValueEventListener personListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> actorSnapshot = dataSnapshot.getChildren();
                for (DataSnapshot person : actorSnapshot) {
                    Person p = person.getValue(Person.class);
                    Log.e("person:: ", p.getName());
                    if (p.getType().equals(DIRECTOR)) {
                        directorArray.add(p.getName());
                    } else if (p.getType().equals(ACTOR)) {
                        actorArray.add(p.getName());
                    }
                }
                convertData();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "loadAccount:onCancelled", databaseError.toException());
            }
        };
        actor_ref.addValueEventListener(personListener);
        director_ref.addValueEventListener(personListener);
    }

    private void matchViewId() {
        edtMovieName = (EditText) findViewById(R.id.edt_name);
        edtManuafactor = (EditText) findViewById(R.id.edt_manuafactor);
        edtDuration = (EditText) findViewById(R.id.edt_duration);
        edtInfo = (EditText) findViewById(R.id.edt_info);
        edtCountry = (EditText) findViewById(R.id.edt_nation);
        tvActors = (TextView) findViewById(R.id.tv_actors);
        tvDirectors = (TextView) findViewById(R.id.tv_directors);
        tvGeneres = (TextView) findViewById(R.id.tv_genere);
        tvReleaseDay = (TextView) findViewById(R.id.tv_release);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        btnSelectActor = (Button) findViewById(R.id.btn_add_actor);
        btnSelectDirector = (Button) findViewById(R.id.btn_add_director);
        btnSelectGenere = (Button) findViewById(R.id.btn_add_genere);
        btnPickDate = (Button) findViewById(R.id.btn_pickdate);
        btnSubmit = (Button) findViewById(R.id.btn_submit);
        imgHolder = (ImageView) findViewById(R.id.img_holder);
        imgThumbnail = (ResponsiveImageView) findViewById(R.id.img_thumbnail);
        rltSelectThumbnail = (RelativeLayout) findViewById(R.id.rlt_thumbnail);
        btnBack = (MaterialRippleLayout) findViewById(R.id.btn_back);
        btnHome = (MaterialRippleLayout) findViewById(R.id.btn_home);
    }

    private void setNavigationEvent() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home_intent = new Intent(AddMovieActivity.this, HomeActivity.class);
                startActivity(home_intent);
                finish();
            }
        });
    }

    private void selectGeneres () {
        btnSelectGenere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSelectItems(generes, selectedGenere, tvGeneres, "Chọn thể loại");
            }
        });
    }

    private void selectActors() {
        btnSelectActor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSelectItems(actors, selectedActor, tvActors, "Chọn diễn viên");
            }
        });
    }

    private void selectDirectors() {
        btnSelectDirector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSelectItems(directors, selectedDirector, tvDirectors, "Chọn đạo diễn");
            }
        });
    }

    private void setDefaultDay() {
        Date date = Calendar.getInstance().getTime();
        tvReleaseDay.setText(getDateFormat(date));
    }

    private void pickDate() {
        btnPickDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });
    }

    private void showDatePicker() {
        DatePickerDialog.OnDateSetListener callback = new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear,
                                  int dayOfMonth) {
                // Update textview by date pick
                tvReleaseDay.setText(
                        (dayOfMonth) + "/" + (monthOfYear + 1) + "/" + year);
            }
        };
        // Set day in DatePicker match with textview
        String s = tvReleaseDay.getText().toString();
        String strArrtmp[] = s.split("/");
        int day = Integer.parseInt(strArrtmp[0]);
        int month = Integer.parseInt(strArrtmp[1]) - 1;
        int year = Integer.parseInt(strArrtmp[2]);
        DatePickerDialog pic = new DatePickerDialog(
                this,
                callback, year, month, day);
        pic.setTitle("Chọn ngày phát hành");
        pic.show();
    }

    protected void onChangeSelectedReceivers(ArrayList<CharSequence> selectedData, TextView tv) {
        StringBuilder stringBuilder = new StringBuilder();

        for(CharSequence genere : selectedData)
            stringBuilder.append( genere + ",");
        tv.setText(stringBuilder.toString());
    }

    protected void showSelectItems(final CharSequence[] data, final ArrayList<CharSequence> selectedData, final TextView tv, String title) {
        boolean[] checkedReceivers = new boolean[data.length];
        int count = data.length;

        for(int i = 0; i < count; i++)
            checkedReceivers[i] = selectedData.contains(data[i]);

        DialogInterface.OnMultiChoiceClickListener receiversDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if(isChecked)
                    selectedData.add(data[which]);
                else
                    selectedData.remove(data[which]);

                onChangeSelectedReceivers(selectedData, tv);
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle(title)
                .setMultiChoiceItems(data, checkedReceivers, receiversDialogListener)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void submit() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitRegisteration();
            }
        });
    }

    private boolean submitRegisteration() {
        if (validateData()) {
            // Add this movie into database
            try {
                if (isChangeImage) {
                    putImageToCloud(imageFile);
                }
                else if (!isChangeImage && isEdit){
                    img_link = movie.getImageURL();
                    saveNewMovie();
                }
            }
            catch (Exception e) {
                Toast.makeText(this, "Chọn ảnh cho phim", Toast.LENGTH_SHORT).show();
            }
        }
        // If have some errors, notify this error to user and return false...
        else {
            Toast.makeText(this, "Có lỗi xảy ra", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void saveNewMovie() {
        ArrayList<String> generesList = new ArrayList<>();
        for(int i=0; i<selectedGenere.size(); i++) {
            generesList.add(selectedGenere.get(i).toString());
        }
        ArrayList<String> actors = new ArrayList<>();
        ArrayList<String> director = new ArrayList<>();
        if (actorArray.size()<1) {
            actors.add("Trống");
        } else {
            for (CharSequence a : actorArray) {
                actors.add(a.toString());
            }
        }
        if (directorArray.size() < 1) {
            director.add("Trống");
        } else {
            for (CharSequence a : directorArray) {
                director.add(a.toString());
            }
        }

        if (!isEdit) {
            Movie newMovie = new Movie(movie_name, generesList, actors, director, release_day, 5.0f, country,
                    duration, manuafactor, info, img_link, comments, globalUser.getUid());
            String movie_id = db_helper.addMovie(newMovie);
            Toast.makeText(this, "Thêm mới phim thành công", Toast.LENGTH_SHORT).show();
        } else {
            Movie newMovie = new Movie(movie_name, generesList, actors, director, release_day, movie.getAvg_rating(), country,
                    duration, manuafactor, info, img_link, comments, globalUser.getUid());
            db_helper.updateMovie(movie_id, newMovie);
            Intent intent = new Intent(this, ContentManagementActivity.class);
            startActivity(intent);
            Toast.makeText(this, "Chỉnh sửa thành công", Toast.LENGTH_SHORT).show();
        }
        clearUI();
    }

    private void clearUI() {
        edtCountry.setText("");
        edtDuration.setText("");
        edtMovieName.setText("");
        edtInfo.setText("");
        edtManuafactor.setText("");
        imgHolder.setVisibility(View.VISIBLE);
        imgThumbnail.setImageResource(0);
        tvDirectors.setText("");
        tvActors.setText("");
        tvGeneres.setText("");
        setDefaultDay();
    }

    public void putImageToCloud(File file) {
        String file_name = System.currentTimeMillis() + file.getName();
        StorageReference imgRef = mStorageRef.child("images/"+file_name);
        showProgressDialog();
        imgRef.putFile(Uri.fromFile(file))
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                        img_link = downloadUrl.toString();
                        Log.e(TAG, img_link);
                        saveNewMovie();
                        hideProgressDialog();
                        Toast.makeText(AddMovieActivity.this, "Upload phim thành công", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Toast.makeText(AddMovieActivity.this, "Đã có lỗi xảy ra", Toast.LENGTH_SHORT).show();
                        hideProgressDialog();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == IMG_RESULT && resultCode == RESULT_OK
                    && null != data) {
                Uri URI = data.getData();
                imageFile = new File(getRealPathFromURI(URI));
                String[] FILE = { MediaStore.Images.Media.DATA };
                Cursor cursor = getContentResolver().query(URI,
                        FILE, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(FILE[0]);
                ImageDecode = cursor.getString(columnIndex);
                cursor.close();
                imgThumbnail.setImageBitmap(BitmapFactory
                        .decodeFile(ImageDecode));
                isChangeImage = true;
            }
        } catch (Exception e) {
            Toast.makeText(this, "Please try again", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    private boolean validateData() {
        getReleaseDay();
        movie_name = edtMovieName.getText().toString();
        manuafactor = edtManuafactor.getText().toString();
        duration = edtDuration.getText().toString();
        info = edtInfo.getText().toString();
        country = edtCountry.getText().toString();
        if (movie_name.isEmpty() || manuafactor.isEmpty() ||
                duration.isEmpty() || info.isEmpty() || country.isEmpty()) {
            Toast.makeText(this, "Điền đầy đủ các trường", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void getReleaseDay() {
         release_day = tvReleaseDay.getText().toString();
    }

    private void browsePicture() {
        intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, IMG_RESULT);
    }
}
