package com.ambition.moviereview.model;

/**
 * Created by nhan on 05/05/2018.
 */

public class Comment {
    // Attribute
    private Account user = new Account();
    private long time_comment = 0;
    private int rating_level = 5;
    private String comment_content = "";

    public Comment() {
    }

    public Comment(Account user, long time_comment, int rating_level, String comment_content) {
        this.user = user;
        this.time_comment = time_comment;
        this.rating_level = rating_level;
        this.comment_content = comment_content;
    }

    public Account getUser() {
        return user;
    }

    public void setUser(Account user) {
        this.user = user;
    }

    public long getTime_comment() {
        return time_comment;
    }

    public void setTime_comment(long time_comment) {
        this.time_comment = time_comment;
    }

    public int getRating_level() {
        return rating_level;
    }

    public void setRating_level(int rating_level) {
        this.rating_level = rating_level;
    }

    public String getComment_content() {
        return comment_content;
    }

    public void setComment_content(String comment_content) {
        this.comment_content = comment_content;
    }
}
