package com.ambition.moviereview.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import android.content.SharedPreferences;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.ambition.moviereview.utilities.Constants.UID;

/**
 * Created by nhan on 05/05/2018.
 */

public class Utilities {

    private static final String TAG = Utilities.class.getSimpleName();
    /** Set custom font */
    public static void setFontFamily(String fontName, TextView textView, Context context) {
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/" + fontName);
        textView.setTypeface(font);
    }

    public static void overrideFonts(final Context context, final View v, String fontName) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child, fontName);
                }
            } else if (v instanceof TextView ) {
                ((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts" + fontName));
            }
        } catch (Exception e) {
            Log.e(TAG, "Have error when change fontfamily");
        }
    }

    public static String formatNumberToPrice(double value) {
        String valuePart = NumberFormat.getNumberInstance(Locale.US).format(value);
        return valuePart + " VND";
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static String getDateFormat(Date d) {
        SimpleDateFormat dft = new
                SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        return dft.format(d);
    }

    public static String getHourFormat(Date d) {
        SimpleDateFormat dft=new
                SimpleDateFormat("hh:mm a", Locale.getDefault());
        return dft.format(d);
    }

    public static boolean checkUsername(String s) {
        Pattern p = Pattern.compile("[a-zA-Z0-9[._]]{6,32}");
        Matcher matcher = p.matcher(s);
        return matcher.matches();
    }

    public static boolean checkEmail(String email){
        Pattern p = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = p.matcher(email);
        return  matcher.find();
    }

    public static String getTimeFromMilisecond(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static Date getDate(String dtStart) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date date = format.parse(dtStart);
            Log.e(TAG, date + "" );
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
