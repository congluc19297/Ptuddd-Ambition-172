package com.ambition.moviereview.utilities;

import android.view.View;

/**
 * Created by nhan on 18/05/2018.
 */

public interface ItemClickListener {
    public void onItemClick(View v, int position);
}
