package com.ambition.moviereview.controller;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ambition.moviereview.R;
import com.ambition.moviereview.model.Comment;
import com.ambition.moviereview.model.Movie;
import com.ambition.moviereview.model.Person;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.ambition.moviereview.utilities.Utilities.getDate;

/**
 * Created by nhan on 20/05/2018.
 */

public class PersonContentAdapter extends ArrayAdapter<Person> {

    public PersonContentAdapter(Context context, ArrayList<Person> data) {
        super(context, 0, data);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // Get the data item for this position
        Person person = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.content_item, parent, false);
        }
        // Link view by id
        TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
        TextView tvContent = (TextView) convertView.findViewById(R.id.tv_content);

        // Update data
        tvTitle.setText(person.getName());
        Date birthday = getDate(person.getBirth_day());
        Date now = Calendar.getInstance().getTime();
        int year_old = now.getYear() - birthday.getYear();
        tvContent.setText(String.valueOf(year_old));
        return convertView;
    }
}
