package com.ambition.moviereview.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.ambition.moviereview.R;
import com.ambition.moviereview.controller.MovieContentAdapter;
import com.ambition.moviereview.controller.PersonContentAdapter;
import com.ambition.moviereview.model.Movie;
import com.ambition.moviereview.model.Person;
import com.balysv.materialripple.MaterialRippleLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static com.ambition.moviereview.utilities.Constants.ACTOR;
import static com.ambition.moviereview.utilities.Constants.DIRECTOR;
import static com.ambition.moviereview.utilities.Constants.IS_EDIT;
import static com.ambition.moviereview.utilities.Constants.KIND;
import static com.ambition.moviereview.utilities.Constants.MOVIE;
import static com.ambition.moviereview.utilities.Constants.MOVIE_ID;
import static com.ambition.moviereview.utilities.Constants.PERSON_ID;

public class ContentManagementActivity extends BaseActivity {
    private final String TAG = ContentManagementActivity.class.getSimpleName();
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference movie_ref = database.getReference(MOVIE);
    private DatabaseReference actor_ref = database.getReference(ACTOR);
    private DatabaseReference director_ref = database.getReference(DIRECTOR);
    private ArrayList<Movie> movies = new ArrayList<>();
    private ArrayList<Person> actors = new ArrayList<>();
    private ArrayList<Person> directors = new ArrayList<>();
    private ListView lvMovies, lvActors, lvDirectors;
    private TextView tvNoMovies, tvNoActor, tvNoDirector;
    private MaterialRippleLayout btnBack, btnHome;
    MovieContentAdapter movieAdapter;
    PersonContentAdapter actorAdapter;
    PersonContentAdapter directorAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_management);

        lvMovies = (ListView) findViewById(R.id.lv_movies);
        lvActors = (ListView) findViewById(R.id.lv_actors);
        lvDirectors = (ListView) findViewById(R.id.lv_directors);
        tvNoMovies = (TextView) findViewById(R.id.tv_no_movie);
        tvNoActor = (TextView) findViewById(R.id.tv_no_actor);
        tvNoDirector = (TextView) findViewById(R.id.tv_no_director);
        btnBack = (MaterialRippleLayout) findViewById(R.id.btn_back);
        btnHome = (MaterialRippleLayout) findViewById(R.id.btn_home);
        setNavigationEvent();
//        movieAdapter = new MovieContentAdapter(this, movies);
//        lvMovies.setAdapter(movieAdapter);

//        actorAdapter = new PersonContentAdapter(this, actors);
//        lvActors.setAdapter(actorAdapter);

//        directorAdapter = new PersonContentAdapter(this, directors);
//        lvDirectors.setAdapter(directorAdapter);



        loadMovies();
        loadPerson();
    }

    private void setNavigationEvent() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home_intent = new Intent(ContentManagementActivity.this, HomeActivity.class);
                startActivity(home_intent);
                finish();
            }
        });
    }

    private void loadMovies() {
        showProgressDialog();
        ValueEventListener movieListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> movieSnapshot = dataSnapshot.getChildren();
                for (DataSnapshot movie : movieSnapshot) {
                    try {
                        Movie m = movie.getValue(Movie.class);
                        if (m.getOwner_uid().equals(globalUser.getUid())) {
                            Log.e("movie_add: ", m.getName());
                            movies.add(m);
                        } else {
                            Log.e("movie_skip: ", m.getName());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                Log.e(TAG, "movies: " + movies.size());
//                movieAdapter.clear();
//                movieAdapter.addAll(movies);
//                movieAdapter.notifyDataSetChanged();
                if (movies.size() < 1) {
                    lvMovies.setVisibility(View.GONE);
                    tvNoMovies.setVisibility(View.VISIBLE);
                }
                else {
                    tvNoMovies.setVisibility(View.GONE);
                    movieAdapter = new MovieContentAdapter(ContentManagementActivity.this, movies);
                    lvMovies.setAdapter(movieAdapter);
                    lvMovies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent movie_intent = new Intent(ContentManagementActivity.this, AddMovieActivity.class);
                            movie_intent.putExtra(IS_EDIT, true);
                            movie_intent.putExtra(MOVIE_ID, movies.get(position).getId());
                            startActivity(movie_intent);
                        }
                    });
                }
                hideProgressDialog();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "loadMovie: failed - ", databaseError.toException());
                hideProgressDialog();
            }
        };
        movie_ref.addListenerForSingleValueEvent(movieListener);
    }

    private void loadPerson() {
        showProgressDialog();
        ValueEventListener personListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> actorSnapshot = dataSnapshot.getChildren();
                for (DataSnapshot person : actorSnapshot) {
                    Person p = person.getValue(Person.class);
                    Log.e("person:: ", p.getName());
                    if (p.getType().equals(DIRECTOR)) {
                        directors.add(p);
                    } else if (p.getType().equals(ACTOR)) {
                        actors.add(p);
                    }
                }
//                actorAdapter.clear();
//                actorAdapter.addAll(actors);
//                actorAdapter.notifyDataSetChanged();
//                directorAdapter.clear();
//                directorAdapter.addAll(directors);
//                directorAdapter.notifyDataSetChanged();
                if (actors.size() < 1) {
                    lvActors.setVisibility(View.GONE);
                    tvNoActor.setVisibility(View.VISIBLE);
                } else {
                    tvNoActor.setVisibility(View.GONE);
                    lvActors.setVisibility(View.VISIBLE);
                    actorAdapter = new PersonContentAdapter(ContentManagementActivity.this, actors);
                    lvActors.setAdapter(actorAdapter);
                    lvActors.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent person_intent = new Intent(ContentManagementActivity.this, AddPersonActivity.class);
                            person_intent.putExtra(IS_EDIT, true);
                            person_intent.putExtra(KIND, ACTOR);
                            person_intent.putExtra(PERSON_ID, actors.get(position).getId());
                            startActivity(person_intent);
                        }
                    });
                }
                if (directors.size() < 1) {
                    lvDirectors.setVisibility(View.GONE);
                    tvNoDirector.setVisibility(View.VISIBLE);
                }
                else {
                    lvDirectors.setVisibility(View.VISIBLE);
                    tvNoDirector.setVisibility(View.GONE);
                    directorAdapter = new PersonContentAdapter(ContentManagementActivity.this, directors);
                    lvDirectors.setAdapter(directorAdapter);
                    lvDirectors.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent person_intent = new Intent(ContentManagementActivity.this, AddPersonActivity.class);
                            person_intent.putExtra(IS_EDIT, true);
                            person_intent.putExtra(KIND, DIRECTOR);
                            person_intent.putExtra(PERSON_ID, directors.get(position).getId());
                            startActivity(person_intent);
                        }
                    });
                }
                hideProgressDialog();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "loadMovie: failed - ", databaseError.toException());
                hideProgressDialog();
            }
        };
        actor_ref.addListenerForSingleValueEvent(personListener);
        director_ref.addListenerForSingleValueEvent(personListener);
    }
}
