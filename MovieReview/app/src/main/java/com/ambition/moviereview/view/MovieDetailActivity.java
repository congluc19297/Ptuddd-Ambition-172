package com.ambition.moviereview.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ambition.moviereview.R;
import com.ambition.moviereview.controller.CommentAdapter;
import com.ambition.moviereview.controller.FireBaseHelper;
import com.ambition.moviereview.model.Account;
import com.ambition.moviereview.model.Comment;
import com.ambition.moviereview.model.Movie;
import com.ambition.moviereview.utilities.ResponsiveImageView;
import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static com.ambition.moviereview.utilities.Constants.ACCOUNT;
import static com.ambition.moviereview.utilities.Constants.MOVIE;
import static com.ambition.moviereview.utilities.Constants.MOVIE_ID;

public class MovieDetailActivity extends BaseActivity {
    // Views
    private ResponsiveImageView imgThumbnail;
    private ImageView imgHolder;
    private TextView tvMovieName, tvDirector, tvActors, tvGenres,
            tvProduction, tvReleaseDay, tvNation, tvDuration, tvUploader, tvInfo, tvRating;
    private RecyclerView rclComments;
    private TextView tvEmpty;
    private ImageView imgStar1, imgStar2, imgStar3, imgStar4, imgStar5;
    private MaterialRippleLayout btnBack, btnHome;
    private FloatingActionButton fabAddComment;
    private AlertDialog alertDialog;

    // Database
    private final String TAG = MovieDetailActivity.class.getSimpleName();
    private String movieId = "";
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    FireBaseHelper db_helper;
    DatabaseReference movie_ref = database.getReference(MOVIE);
    DatabaseReference user_ref = database.getReference(ACCOUNT);
    private Movie movie = new Movie();
    private Account account = new Account();
    private String comment_content = "";

    private int comment_rating = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        db_helper = new FireBaseHelper(this);
        matchViewId();
        setNavigationEvent();
        movieId = getIntent().getStringExtra(MOVIE_ID);
        loadData();
        setFABEvent();
    }

    private void setFABEvent() {
        fabAddComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLogged()) {
                    showAddCommentDialog();
                } else {
                    goSignIn();
                }
            }
        });
    }

    private void showAddCommentDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_add_comment, null);
        dialogBuilder.setView(dialogView);

        final ImageView mStar1 = (ImageView) dialogView.findViewById(R.id.img_s1);
        final ImageView mStar2 = (ImageView) dialogView.findViewById(R.id.img_s2);
        final ImageView mStar3 = (ImageView) dialogView.findViewById(R.id.img_s3);
        final ImageView mStar4 = (ImageView) dialogView.findViewById(R.id.img_s4);
        final ImageView mStar5 = (ImageView) dialogView.findViewById(R.id.img_s5);

        // Set event
        mStar1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment_rating = 1;
                mStar1.setImageResource(R.drawable.ic_star_full);
                mStar2.setImageResource(R.drawable.ic_empty);
                mStar3.setImageResource(R.drawable.ic_empty);
                mStar4.setImageResource(R.drawable.ic_empty);
                mStar5.setImageResource(R.drawable.ic_empty);
            }
        });
        mStar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment_rating = 2;
                mStar1.setImageResource(R.drawable.ic_star_full);
                mStar2.setImageResource(R.drawable.ic_star_full);
                mStar3.setImageResource(R.drawable.ic_empty);
                mStar4.setImageResource(R.drawable.ic_empty);
                mStar5.setImageResource(R.drawable.ic_empty);
            }
        });
        mStar3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment_rating = 3;
                mStar1.setImageResource(R.drawable.ic_star_full);
                mStar2.setImageResource(R.drawable.ic_star_full);
                mStar3.setImageResource(R.drawable.ic_star_full);
                mStar4.setImageResource(R.drawable.ic_empty);
                mStar5.setImageResource(R.drawable.ic_empty);
            }
        });
        mStar4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment_rating = 4;
                mStar1.setImageResource(R.drawable.ic_star_full);
                mStar2.setImageResource(R.drawable.ic_star_full);
                mStar3.setImageResource(R.drawable.ic_star_full);
                mStar4.setImageResource(R.drawable.ic_star_full);
                mStar5.setImageResource(R.drawable.ic_empty);
            }
        });
        mStar5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment_rating = 5;
                mStar1.setImageResource(R.drawable.ic_star_full);
                mStar2.setImageResource(R.drawable.ic_star_full);
                mStar3.setImageResource(R.drawable.ic_star_full);
                mStar4.setImageResource(R.drawable.ic_star_full);
                mStar5.setImageResource(R.drawable.ic_star_full);
            }
        });
        final EditText edtComment = (EditText) dialogView.findViewById(R.id.edt_content);
        dialogBuilder.setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                comment_content = edtComment.getText().toString();
                addComment(globalUser.getUid());
            }
        });
        alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    private void addComment(String uid) {
        ValueEventListener userUpload = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    account = dataSnapshot.getValue(Account.class);
                    long time = System.currentTimeMillis();
                    Comment new_comment = new Comment(account, time, comment_rating, comment_content);
                    db_helper.addComment(new_comment, movie);
                } catch (Exception e) {
                    Log.e(TAG, "Get uploader error" + e.toString());
                }
                hideProgressDialog();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "loadMovie: failed - ", databaseError.toException());
                hideProgressDialog();
            }
        };
        user_ref.child(uid).addListenerForSingleValueEvent(userUpload);
    }

    private void updateUI() {
        // Thumbnail
        Glide.with(this)
                .load(movie.getImageURL())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        imgHolder.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(imgThumbnail);
        // Movie name
        tvMovieName.setText(movie.getName());
        // Directors
        String directors = "Đạo diễn: | ";
        for (String director: movie.getDirector()) {
            directors += director + " | ";
        }
        try {
            tvDirector.setText(directors);
        }
        catch (Exception e) {
            tvDirector.setText("Đạo diễn: Chưa có");
        }
        // Genres
        String genres = "Thể loại: | ";
        if (movie.getGenres().size() != 0) {
            for (String genre : movie.getGenres()) {
                genres += genre + " | ";
            }
        }
        try {
            tvGenres.setText(genres);
        }
        catch (Exception e) {
            tvGenres.setText("Thể loại: Chưa có");
        }
        // Actors
        String actors = "Diễn viên: | ";
        for (String actor: movie.getActors()) {
            actors += actor + " | ";
        }
        try {
            tvActors.setText(actors);
        }
        catch (Exception e) {
            tvActors.setText("Diễn viên: Chưa có");
        }
        // Others
        tvProduction.setText("Hãng sản xuất: " + movie.getProduction_company());
        tvReleaseDay.setText("Ngày phát hành: " + movie.getRelease_date());
        tvDuration.setText("Thời lượng: " + movie.getDuration() +" phút");
        tvInfo.setText(movie.getContent());
        tvNation.setText("Quốc gia: " + movie.getCountry());
        setTextUploader(movie.getOwner_uid());

        float rating = movie.getAvg_rating();
        tvRating.setText("Đánh giá: " + String.valueOf(rating));
        if (rating < 0.5) {
            imgStar1.setImageResource(R.drawable.ic_empty);
            imgStar2.setImageResource(R.drawable.ic_empty);
            imgStar3.setImageResource(R.drawable.ic_empty);
            imgStar4.setImageResource(R.drawable.ic_empty);
            imgStar5.setImageResource(R.drawable.ic_empty);
        } else if (rating >= 0.5f && rating < 0.75f) {
            imgStar1.setImageResource(R.drawable.ic_star_half);
            imgStar2.setImageResource(R.drawable.ic_empty);
            imgStar3.setImageResource(R.drawable.ic_empty);
            imgStar4.setImageResource(R.drawable.ic_empty);
            imgStar5.setImageResource(R.drawable.ic_empty);
        } else if (rating >= 0.75 && rating < 1.25) {
            imgStar1.setImageResource(R.drawable.ic_star_full);
            imgStar2.setImageResource(R.drawable.ic_empty);
            imgStar3.setImageResource(R.drawable.ic_empty);
            imgStar4.setImageResource(R.drawable.ic_empty);
            imgStar5.setImageResource(R.drawable.ic_empty);
        }
        else if (rating >= 1.25f && rating <1.75f) {
            imgStar1.setImageResource(R.drawable.ic_star_full);
            imgStar2.setImageResource(R.drawable.ic_star_half);
            imgStar3.setImageResource(R.drawable.ic_empty);
            imgStar4.setImageResource(R.drawable.ic_empty);
            imgStar5.setImageResource(R.drawable.ic_empty);
        }
        else if (rating >= 1.75 && rating < 2.25) {
            imgStar1.setImageResource(R.drawable.ic_star_full);
            imgStar2.setImageResource(R.drawable.ic_star_full);
            imgStar3.setImageResource(R.drawable.ic_empty);
            imgStar4.setImageResource(R.drawable.ic_empty);
            imgStar5.setImageResource(R.drawable.ic_empty);
        }
        else if (rating >= 2.25f && rating <2.75f) {
            imgStar1.setImageResource(R.drawable.ic_star_full);
            imgStar2.setImageResource(R.drawable.ic_star_full);
            imgStar3.setImageResource(R.drawable.ic_star_half);
            imgStar4.setImageResource(R.drawable.ic_empty);
            imgStar5.setImageResource(R.drawable.ic_empty);
        }
        else if (rating >= 2.75 && rating < 3.25) {
            imgStar1.setImageResource(R.drawable.ic_star_full);
            imgStar2.setImageResource(R.drawable.ic_star_full);
            imgStar3.setImageResource(R.drawable.ic_star_full);
            imgStar4.setImageResource(R.drawable.ic_empty);
            imgStar5.setImageResource(R.drawable.ic_empty);
        }
        else if (rating >= 3.25f && rating <3.75f) {
            imgStar1.setImageResource(R.drawable.ic_star_full);
            imgStar2.setImageResource(R.drawable.ic_star_full);
            imgStar3.setImageResource(R.drawable.ic_star_full);
            imgStar4.setImageResource(R.drawable.ic_star_half);
            imgStar5.setImageResource(R.drawable.ic_empty);
        }
        else if (rating >= 3.75 && rating < 4.25) {
            imgStar1.setImageResource(R.drawable.ic_star_full);
            imgStar2.setImageResource(R.drawable.ic_star_full);
            imgStar3.setImageResource(R.drawable.ic_star_full);
            imgStar4.setImageResource(R.drawable.ic_star_full);
            imgStar5.setImageResource(R.drawable.ic_empty);
        }
        else if (rating >= 4.25f && rating <4.75f) {
            imgStar1.setImageResource(R.drawable.ic_star_full);
            imgStar2.setImageResource(R.drawable.ic_star_full);
            imgStar3.setImageResource(R.drawable.ic_star_full);
            imgStar4.setImageResource(R.drawable.ic_star_full);
            imgStar5.setImageResource(R.drawable.ic_star_half);
        }
        else {
            imgStar1.setImageResource(R.drawable.ic_star_full);
            imgStar2.setImageResource(R.drawable.ic_star_full);
            imgStar3.setImageResource(R.drawable.ic_star_full);
            imgStar4.setImageResource(R.drawable.ic_star_full);
            imgStar5.setImageResource(R.drawable.ic_star_full);
        }
    }

    private void setTextUploader(String owner_uid) {
        ValueEventListener userUpload = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    Account uploader = dataSnapshot.getValue(Account.class);
                    tvUploader.setText(uploader.getName());
                } catch (Exception e) {
                    Log.e(TAG, "Get uploader error" + e.toString());
                }
                hideProgressDialog();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "loadMovie: failed - ", databaseError.toException());
                hideProgressDialog();
            }
        };
        user_ref.child(owner_uid).addListenerForSingleValueEvent(userUpload);
    }

    private void matchViewId() {
        imgThumbnail = (ResponsiveImageView) findViewById(R.id.img_thumbnail);
        imgHolder = (ImageView) findViewById(R.id.img_holder);
        tvMovieName = (TextView) findViewById(R.id.movie_name);
        tvDirector = (TextView) findViewById(R.id.tv_directors);
        tvGenres = (TextView) findViewById(R.id.tv_genres);
        tvActors = (TextView) findViewById(R.id.tv_actors);
        tvProduction = (TextView) findViewById(R.id.tv_production);
        tvReleaseDay = (TextView) findViewById(R.id.tv_release);
        tvNation = (TextView) findViewById(R.id.tv_nation);
        tvDuration = (TextView) findViewById(R.id.tv_duration);
        tvUploader = (TextView) findViewById(R.id.tv_uploader);
        tvInfo = (TextView) findViewById(R.id.tv_info);
        tvRating = (TextView) findViewById(R.id.tv_rating);
        tvEmpty = (TextView) findViewById(R.id.tv_no_comment);
        btnBack = (MaterialRippleLayout) findViewById(R.id.btn_back);
        btnHome = (MaterialRippleLayout) findViewById(R.id.btn_home);
        rclComments = (RecyclerView) findViewById(R.id.lv_comments);
        imgStar1 = (ImageView) findViewById(R.id.img_s1);
        imgStar2 = (ImageView) findViewById(R.id.img_s2);
        imgStar3 = (ImageView) findViewById(R.id.img_s3);
        imgStar4 = (ImageView) findViewById(R.id.img_s4);
        imgStar5 = (ImageView) findViewById(R.id.img_s5);
        fabAddComment = (FloatingActionButton) findViewById(R.id.fab);
    }

    private void setNavigationEvent() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home_intent = new Intent(MovieDetailActivity.this, HomeActivity.class);
                startActivity(home_intent);
                finish();
            }
        });
    }

    private void loadData() {
        showProgressDialog();
        ValueEventListener movieListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    movie = dataSnapshot.getValue(Movie.class);
                    Log.e(TAG, movie.getName());
                    updateUI();
                    updateComments();
                } catch (Exception e) {
                    Log.e(TAG, "Parse Movie went wrong, error: " + e.toString());
                }
                hideProgressDialog();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "loadMovie: failed - ", databaseError.toException());
                hideProgressDialog();
            }
        };
        movie_ref.child(movieId).addValueEventListener(movieListener);
    }

    private void updateComments() {
        ArrayList<Comment> comments = movie.getComments();
        if (comments.size() < 1) {
            tvEmpty.setVisibility(View.VISIBLE);
            rclComments.setVisibility(View.GONE);
        } else {
            tvEmpty.setVisibility(View.GONE);
            rclComments.setVisibility(View.VISIBLE);
            CommentAdapter commentAdapter = new CommentAdapter(comments);
            rclComments.setAdapter(commentAdapter);
            rclComments.setLayoutManager(new LinearLayoutManager(this));
        }
    }

}
